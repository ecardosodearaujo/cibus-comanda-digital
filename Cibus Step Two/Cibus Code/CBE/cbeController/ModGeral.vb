﻿Imports System.Data.SqlClient
Imports System.Text
Imports System.Windows.Forms
Imports System.Reflection
Imports System.Text.RegularExpressions
Imports System.IO
Imports Microsoft.Win32
Imports System.Globalization
Module ModGeral
    'Inicio do projeto: Everaldo Cardoso de Araujo 28/07/2015

    'Variaveis de conexão:
    Public sqlConn As New SqlConnection
    Public sSenhaBD As String = "M1n3Rv@7"
    Public sUsuarioBD As String = "SA"
    Public sNomeBD As String = "BDCBS"
    Public sNomeServerBD As String = "JAVALI\CARDOSO"
    'Demais variaveis:
    Public sUsuario As String = ""
    Public sCodUsuario As String = ""
    Public sUsuarioNome As String = ""
    Public sUsuarioSobrenome As String = ""
    Public sCodEmpresa As String = ""
    Public sEmpresa As String = ""
    

    Public Function ExecuteReader(ByVal SbSql As StringBuilder) As SqlDataReader
        Dim cmd As SqlCommand
        Dim dr As SqlDataReader
        Try
            cmd = New SqlCommand(SbSql.ToString, sqlConn)
            dr = cmd.ExecuteReader
            Return dr
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            cmd.Dispose()
            cmd = Nothing
            SbSql = Nothing
        End Try
    End Function

    Public Function ExecuteNonQuery(ByVal SbSql As StringBuilder) As Boolean
        Dim cmd As SqlCommand
        Try
            cmd = New SqlCommand(SbSql.ToString, sqlConn)
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        Finally
            cmd.Dispose()
            cmd = Nothing
            SbSql = Nothing
        End Try
    End Function
End Module
