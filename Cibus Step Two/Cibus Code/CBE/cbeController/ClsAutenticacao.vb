﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Text

Public Class ClsAutenticacao
    Private frmAutenticacao As Form
    Private txtUsuario As TextBox
    Private txtSenha As TextBox
    Private txtEmpresaID As TextBox
    Private txtDscEmp As TextBox
    Private btnAutenticar As Button
    Private btnCancelar As Button

    Public Sub New(ByVal frmAutenticacao As Form, txtUsuario As TextBox, txtSenha As TextBox, txtEmpresaID As TextBox, txtDscEmp As TextBox, btnAutenticar As Button, btnCancelar As Button)
        Me.frmAutenticacao = frmAutenticacao
        Me.txtUsuario = txtUsuario
        Me.txtSenha = txtSenha
        Me.txtEmpresaID = txtEmpresaID
        Me.txtDscEmp = txtDscEmp
        Me.btnAutenticar = btnAutenticar
        Me.btnCancelar = btnCancelar
    End Sub

    Public Sub Autenticar()
        Try
            frmAutenticacao.Cursor = Cursors.Default
            frmAutenticacao.Enabled = False

            If ValidaAutenticacao() = False Then Exit Sub

            'Chama da SP: CBS_SP_AUTENTICAR
            Dim cmd As New SqlCommand("CBS_SP_AUTENTICAR", sqlConn)
            'Paramentros de entrada:
            cmd.Parameters.Add(New SqlParameter("@USU_USUARIO", SqlDbType.NVarChar, 200)).Value = txtUsuario.Text.Trim
            cmd.Parameters.Add(New SqlParameter("@USU_SENHA", SqlDbType.NVarChar, 200)).Value = txtSenha.Text.Trim
            cmd.Parameters.Add(New SqlParameter("@EMP_EMPRESAID", SqlDbType.Int)).Value = txtEmpresaID.Text.Trim
            'Paramentros de saida:
            cmd.Parameters.Add(New SqlParameter("@STATUS", SqlDbType.NVarChar, 200)).Direction = ParameterDirection.Output
            cmd.Parameters.Add(New SqlParameter("@RET", SqlDbType.Int)).Direction = ParameterDirection.Output
            cmd.Parameters.Add(New SqlParameter("@USU_USUARIOID_RET", SqlDbType.Int)).Direction = ParameterDirection.Output
            cmd.Parameters.Add(New SqlParameter("@USU_USUARIO_RET", SqlDbType.NVarChar, 200)).Direction = ParameterDirection.Output
            cmd.Parameters.Add(New SqlParameter("@USU_NOME_RET", SqlDbType.NVarChar, 200)).Direction = ParameterDirection.Output
            cmd.Parameters.Add(New SqlParameter("@USU_SOBRENOME_RET", SqlDbType.NVarChar, 400)).Direction = ParameterDirection.Output
            cmd.Parameters.Add(New SqlParameter("@EMP_EMPRESAID_RET", SqlDbType.Int)).Direction = ParameterDirection.Output
            cmd.Parameters.Add(New SqlParameter("@EMP_EMPRESA_RET", SqlDbType.NVarChar, 200)).Direction = ParameterDirection.Output
            'Execução:
            cmd.CommandType = CommandType.StoredProcedure
            cmd.ExecuteNonQuery()

            'Verificando os retornos:
            If cmd.Parameters.Item("@RET").Value = 1 Or cmd.Parameters.Item("@RET").Value = 2 Or cmd.Parameters.Item("@RET").Value = 3 Then 'Verificando se houve algum erro:
                MessageBox.Show(cmd.Parameters.Item("@STATUS").Value.ToString, frmAutenticacao.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            ElseIf cmd.Parameters.Item("@RET").Value = 4 Then 'Usuário validado:

                'Carrega as variaveis globais:
                sCodUsuario = cmd.Parameters.Item("@USU_USUARIOID_RET").Value
                sUsuario = cmd.Parameters.Item("@USU_USUARIO_RET").Value
                sUsuarioNome = cmd.Parameters.Item("@USU_NOME_RET").Value
                sUsuarioSobrenome = cmd.Parameters.Item("@USU_SOBRENOME_RET").Value
                sCodEmpresa = cmd.Parameters.Item("@EMP_EMPRESAID_RET").Value
                sEmpresa = cmd.Parameters.Item("@EMP_EMPRESA_RET").Value

                frmAutenticacao.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, frmAutenticacao.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Finally
            frmAutenticacao.Cursor = Cursors.Default
            frmAutenticacao.Enabled = True
        End Try
    End Sub
    Public Sub BuscarEmpresa()
        Try
            frmAutenticacao.Cursor = Cursors.WaitCursor
            frmAutenticacao.Enabled = False

            If txtEmpresaID.Text.Trim = "" Then
                txtEmpresaID.Clear()
                txtDscEmp.Clear()
                Exit Sub
            End If

            If IsNumeric(txtEmpresaID.Text.Trim) = False Then
                txtEmpresaID.Clear()
                txtDscEmp.Clear()
                Exit Sub
            End If

            Dim sbSql As New StringBuilder
            sbSql.AppendLine(" SELECT ")
            sbSql.AppendLine(" 	EMP_EMPRESAID, ")
            sbSql.AppendLine(" 	EMP_RAZAOSOCIAL ")
            sbSql.AppendLine(" FROM CBS_EMPRESAS  ")
            sbSql.AppendLine(" WHERE EMP_EMPRESAID=" & txtEmpresaID.Text.Trim)
            sbSql.AppendLine(" AND EMP_ATIVO=1 ")

            Dim cmd As New SqlCommand(sbSql.ToString, sqlConn)
            Dim dr As SqlDataReader = cmd.ExecuteReader

            If dr.Read Then
                txtEmpresaID.Text = dr("EMP_EMPRESAID").ToString
                txtDscEmp.Text = dr("EMP_RAZAOSOCIAL").ToString
            Else
                txtEmpresaID.Clear()
                txtDscEmp.Clear()
            End If

            cmd.Dispose()
            sbSql = Nothing
            cmd = Nothing
        Catch ex As Exception
            MessageBox.Show(ex.Message, frmAutenticacao.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Finally
            frmAutenticacao.Cursor = Cursors.Default
            frmAutenticacao.Enabled = True
        End Try
    End Sub
    Public Sub LimpaCampos()
        Try
            txtEmpresaID.Clear()
            txtDscEmp.Clear()
            txtSenha.Clear()
            txtUsuario.Clear()
            txtUsuario.Focus()
        Catch ex As Exception
            MessageBox.Show(ex.Message, frmAutenticacao.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Public Sub Fechar()
        Try
            frmAutenticacao.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, frmAutenticacao.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Private Function ValidaAutenticacao() As Boolean
        Try
            If txtUsuario.Text.Trim = "" Then
                txtUsuario.Focus()
                Return False
            ElseIf txtSenha.Text.Trim = "" Then
                txtSenha.Focus()
                Return False
            ElseIf txtEmpresaID.Text.Trim = "" Then
                txtEmpresaID.Focus()
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, frmAutenticacao.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
        End Try
    End Function
End Class
