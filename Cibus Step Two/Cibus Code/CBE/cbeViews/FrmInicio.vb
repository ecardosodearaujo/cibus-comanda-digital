﻿Public Class FrmInicio

    Private Sub FrmInicio_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Dim frmAutenticacao As New FrmAutenticacao()
            With frmAutenticacao
                .ShowDialog()
                .BringToFront()
                If .Logou = False Then
                    Me.Close()
                End If
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub Fechar()
        Try
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub SairToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SairToolStripMenuItem.Click
        Try
            Call Fechar()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
End Class