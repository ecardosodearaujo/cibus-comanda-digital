﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmInicio
    Inherits System.Windows.Forms.Form

    'Descartar substituições de formulário para limpar a lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Exigido pelo Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'OBSERVAÇÃO: O procedimento a seguir é exigido pelo Windows Form Designer
    'Ele pode ser modificado usando o Windows Form Designer.  
    'Não o modifique usando o editor de códigos.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmInicio))
        Me.tssInicio = New System.Windows.Forms.StatusStrip()
        Me.lblUsuario = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblEmpresa = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblBancoDeDados = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel3 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel4 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.PrincipalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReservasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PedidosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MinhaContaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CadastrosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EmpresasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TipoDeProdutosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MesasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PagamentoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FormasDePagamentoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TiposDePagamentoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BandeirasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegionalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NaçãoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EstadoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MunicipiosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CredenciaisToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsuáriosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FranquiasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EmpresasToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.PropriedadesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuGeral = New System.Windows.Forms.ToolStripMenuItem()
        Me.SairToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TrocarDeEmpresaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BloquearToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.tssInicio.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'tssInicio
        '
        Me.tssInicio.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblUsuario, Me.ToolStripStatusLabel1, Me.lblEmpresa, Me.ToolStripStatusLabel2, Me.lblBancoDeDados, Me.ToolStripStatusLabel3, Me.ToolStripStatusLabel4})
        Me.tssInicio.Location = New System.Drawing.Point(0, 411)
        Me.tssInicio.Name = "tssInicio"
        Me.tssInicio.Size = New System.Drawing.Size(764, 22)
        Me.tssInicio.TabIndex = 2
        Me.tssInicio.Text = "StatusStrip1"
        '
        'lblUsuario
        '
        Me.lblUsuario.Name = "lblUsuario"
        Me.lblUsuario.Size = New System.Drawing.Size(98, 17)
        Me.lblUsuario.Text = "Usuário: Everaldo"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(13, 17)
        Me.ToolStripStatusLabel1.Text = "::"
        '
        'lblEmpresa
        '
        Me.lblEmpresa.Name = "lblEmpresa"
        Me.lblEmpresa.Size = New System.Drawing.Size(177, 17)
        Me.lblEmpresa.Text = "Empresa: Cibus comanda digital"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(13, 17)
        Me.ToolStripStatusLabel2.Text = "::"
        '
        'lblBancoDeDados
        '
        Me.lblBancoDeDados.Name = "lblBancoDeDados"
        Me.lblBancoDeDados.Size = New System.Drawing.Size(190, 17)
        Me.lblBancoDeDados.Text = "Fonte de dados: JAVALI\CARDOSO"
        '
        'ToolStripStatusLabel3
        '
        Me.ToolStripStatusLabel3.Name = "ToolStripStatusLabel3"
        Me.ToolStripStatusLabel3.Size = New System.Drawing.Size(13, 17)
        Me.ToolStripStatusLabel3.Text = "::"
        '
        'ToolStripStatusLabel4
        '
        Me.ToolStripStatusLabel4.Name = "ToolStripStatusLabel4"
        Me.ToolStripStatusLabel4.Size = New System.Drawing.Size(133, 17)
        Me.ToolStripStatusLabel4.Text = "Banco de dados: BDCBS"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PrincipalToolStripMenuItem, Me.CadastrosToolStripMenuItem, Me.mnuGeral})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(764, 24)
        Me.MenuStrip1.TabIndex = 3
        Me.MenuStrip1.Text = "msMenus"
        '
        'PrincipalToolStripMenuItem
        '
        Me.PrincipalToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ReservasToolStripMenuItem, Me.PedidosToolStripMenuItem, Me.MinhaContaToolStripMenuItem})
        Me.PrincipalToolStripMenuItem.Name = "PrincipalToolStripMenuItem"
        Me.PrincipalToolStripMenuItem.Size = New System.Drawing.Size(65, 20)
        Me.PrincipalToolStripMenuItem.Text = "Principal"
        '
        'ReservasToolStripMenuItem
        '
        Me.ReservasToolStripMenuItem.Name = "ReservasToolStripMenuItem"
        Me.ReservasToolStripMenuItem.Size = New System.Drawing.Size(141, 22)
        Me.ReservasToolStripMenuItem.Text = "Reservas"
        '
        'PedidosToolStripMenuItem
        '
        Me.PedidosToolStripMenuItem.Name = "PedidosToolStripMenuItem"
        Me.PedidosToolStripMenuItem.Size = New System.Drawing.Size(141, 22)
        Me.PedidosToolStripMenuItem.Text = "Pedidos"
        '
        'MinhaContaToolStripMenuItem
        '
        Me.MinhaContaToolStripMenuItem.Name = "MinhaContaToolStripMenuItem"
        Me.MinhaContaToolStripMenuItem.Size = New System.Drawing.Size(141, 22)
        Me.MinhaContaToolStripMenuItem.Text = "Minha conta"
        '
        'CadastrosToolStripMenuItem
        '
        Me.CadastrosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EmpresasToolStripMenuItem, Me.TipoDeProdutosToolStripMenuItem, Me.MesasToolStripMenuItem, Me.PagamentoToolStripMenuItem, Me.RegionalToolStripMenuItem, Me.CredenciaisToolStripMenuItem, Me.UsuáriosToolStripMenuItem, Me.FranquiasToolStripMenuItem, Me.EmpresasToolStripMenuItem1, Me.PropriedadesToolStripMenuItem})
        Me.CadastrosToolStripMenuItem.Name = "CadastrosToolStripMenuItem"
        Me.CadastrosToolStripMenuItem.Size = New System.Drawing.Size(71, 20)
        Me.CadastrosToolStripMenuItem.Text = "Cadastros"
        '
        'EmpresasToolStripMenuItem
        '
        Me.EmpresasToolStripMenuItem.Name = "EmpresasToolStripMenuItem"
        Me.EmpresasToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.EmpresasToolStripMenuItem.Text = "Produtos"
        '
        'TipoDeProdutosToolStripMenuItem
        '
        Me.TipoDeProdutosToolStripMenuItem.Name = "TipoDeProdutosToolStripMenuItem"
        Me.TipoDeProdutosToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.TipoDeProdutosToolStripMenuItem.Text = "Tipo de produtos"
        '
        'MesasToolStripMenuItem
        '
        Me.MesasToolStripMenuItem.Name = "MesasToolStripMenuItem"
        Me.MesasToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.MesasToolStripMenuItem.Text = "Mesas"
        '
        'PagamentoToolStripMenuItem
        '
        Me.PagamentoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FormasDePagamentoToolStripMenuItem, Me.TiposDePagamentoToolStripMenuItem, Me.BandeirasToolStripMenuItem})
        Me.PagamentoToolStripMenuItem.Name = "PagamentoToolStripMenuItem"
        Me.PagamentoToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.PagamentoToolStripMenuItem.Text = "Pagamento"
        '
        'FormasDePagamentoToolStripMenuItem
        '
        Me.FormasDePagamentoToolStripMenuItem.Name = "FormasDePagamentoToolStripMenuItem"
        Me.FormasDePagamentoToolStripMenuItem.Size = New System.Drawing.Size(193, 22)
        Me.FormasDePagamentoToolStripMenuItem.Text = "Formas de pagamento"
        '
        'TiposDePagamentoToolStripMenuItem
        '
        Me.TiposDePagamentoToolStripMenuItem.Name = "TiposDePagamentoToolStripMenuItem"
        Me.TiposDePagamentoToolStripMenuItem.Size = New System.Drawing.Size(193, 22)
        Me.TiposDePagamentoToolStripMenuItem.Text = "Tipos de pagamento"
        '
        'BandeirasToolStripMenuItem
        '
        Me.BandeirasToolStripMenuItem.Name = "BandeirasToolStripMenuItem"
        Me.BandeirasToolStripMenuItem.Size = New System.Drawing.Size(193, 22)
        Me.BandeirasToolStripMenuItem.Text = "Bandeiras"
        '
        'RegionalToolStripMenuItem
        '
        Me.RegionalToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NaçãoToolStripMenuItem, Me.EstadoToolStripMenuItem, Me.MunicipiosToolStripMenuItem})
        Me.RegionalToolStripMenuItem.Name = "RegionalToolStripMenuItem"
        Me.RegionalToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.RegionalToolStripMenuItem.Text = "Conf. Regionais"
        '
        'NaçãoToolStripMenuItem
        '
        Me.NaçãoToolStripMenuItem.Name = "NaçãoToolStripMenuItem"
        Me.NaçãoToolStripMenuItem.Size = New System.Drawing.Size(116, 22)
        Me.NaçãoToolStripMenuItem.Text = "Nação"
        '
        'EstadoToolStripMenuItem
        '
        Me.EstadoToolStripMenuItem.Name = "EstadoToolStripMenuItem"
        Me.EstadoToolStripMenuItem.Size = New System.Drawing.Size(116, 22)
        Me.EstadoToolStripMenuItem.Text = "Estado"
        '
        'MunicipiosToolStripMenuItem
        '
        Me.MunicipiosToolStripMenuItem.Name = "MunicipiosToolStripMenuItem"
        Me.MunicipiosToolStripMenuItem.Size = New System.Drawing.Size(116, 22)
        Me.MunicipiosToolStripMenuItem.Text = "Cidades"
        '
        'CredenciaisToolStripMenuItem
        '
        Me.CredenciaisToolStripMenuItem.Name = "CredenciaisToolStripMenuItem"
        Me.CredenciaisToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.CredenciaisToolStripMenuItem.Text = "Credenciais"
        '
        'UsuáriosToolStripMenuItem
        '
        Me.UsuáriosToolStripMenuItem.Name = "UsuáriosToolStripMenuItem"
        Me.UsuáriosToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.UsuáriosToolStripMenuItem.Text = "Usuários"
        '
        'FranquiasToolStripMenuItem
        '
        Me.FranquiasToolStripMenuItem.Name = "FranquiasToolStripMenuItem"
        Me.FranquiasToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.FranquiasToolStripMenuItem.Text = "Franquias"
        '
        'EmpresasToolStripMenuItem1
        '
        Me.EmpresasToolStripMenuItem1.Name = "EmpresasToolStripMenuItem1"
        Me.EmpresasToolStripMenuItem1.Size = New System.Drawing.Size(165, 22)
        Me.EmpresasToolStripMenuItem1.Text = "Empresas"
        '
        'PropriedadesToolStripMenuItem
        '
        Me.PropriedadesToolStripMenuItem.Name = "PropriedadesToolStripMenuItem"
        Me.PropriedadesToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.PropriedadesToolStripMenuItem.Text = "Propriedades"
        '
        'mnuGeral
        '
        Me.mnuGeral.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BloquearToolStripMenuItem, Me.TrocarDeEmpresaToolStripMenuItem, Me.SairToolStripMenuItem})
        Me.mnuGeral.Name = "mnuGeral"
        Me.mnuGeral.Size = New System.Drawing.Size(46, 20)
        Me.mnuGeral.Text = "Geral"
        '
        'SairToolStripMenuItem
        '
        Me.SairToolStripMenuItem.Name = "SairToolStripMenuItem"
        Me.SairToolStripMenuItem.Size = New System.Drawing.Size(172, 22)
        Me.SairToolStripMenuItem.Text = "Sair"
        '
        'TrocarDeEmpresaToolStripMenuItem
        '
        Me.TrocarDeEmpresaToolStripMenuItem.Name = "TrocarDeEmpresaToolStripMenuItem"
        Me.TrocarDeEmpresaToolStripMenuItem.Size = New System.Drawing.Size(172, 22)
        Me.TrocarDeEmpresaToolStripMenuItem.Text = "Trocar de Empresa"
        '
        'BloquearToolStripMenuItem
        '
        Me.BloquearToolStripMenuItem.Name = "BloquearToolStripMenuItem"
        Me.BloquearToolStripMenuItem.Size = New System.Drawing.Size(172, 22)
        Me.BloquearToolStripMenuItem.Text = "Bloquear"
        '
        'FrmInicio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(764, 433)
        Me.Controls.Add(Me.tssInicio)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "FrmInicio"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Inicio"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.tssInicio.ResumeLayout(False)
        Me.tssInicio.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tssInicio As System.Windows.Forms.StatusStrip
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents PrincipalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CadastrosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuGeral As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReservasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PedidosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CredenciaisToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EmpresasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MesasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TipoDeProdutosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FranquiasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UsuáriosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RegionalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NaçãoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EstadoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MunicipiosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PagamentoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FormasDePagamentoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TiposDePagamentoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BandeirasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PropriedadesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblUsuario As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents MinhaContaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EmpresasToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblEmpresa As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblBancoDeDados As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel3 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel4 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents SairToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TrocarDeEmpresaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BloquearToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
