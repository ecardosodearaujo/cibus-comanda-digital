﻿Imports System.Data.SqlClient
Imports System.Text
Imports System.Windows.Forms
Imports System.Net.Mail
Imports System.Reflection
Imports System.Text.RegularExpressions
Imports System.IO
Imports Microsoft.Win32
Imports System.Globalization
Public Class FrmAutenticacao
    Private clsAutenticacao As ClsAutenticacao
    Private bLogou As Boolean
    Public Property Logou() As Boolean
        Get
            Return bLogou
        End Get
        Set(ByVal value As Boolean)
            bLogou = value
        End Set
    End Property

    Private Sub FrmAutenticacao_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            'Pegando as informações da conexão para o programa:
            Dim ClsBDConexao As New ClsBDConexao
            Call ClsBDConexao.openDataBase()

            'Passando os dados para a classe de autenticação:
            clsAutenticacao = New ClsAutenticacao(Me, txtUsuario, txtSenha, txtEmpresaID, txtDscEmp, btnAutenticar, btnCancelar)
            Call clsAutenticacao.LimpaCampos()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub btnAutenticar_Click(sender As Object, e As EventArgs) Handles btnAutenticar.Click
        Try
            bLogou = True
            Call clsAutenticacao.Autenticar()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub txtCodEmp_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtEmpresaID.Validating
        Try
            Call clsAutenticacao.BuscarEmpresa()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Try
            bLogou = False
            Call clsAutenticacao.Fechar()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub FrmAutenticacao_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Escape Then
                bLogou = False
                clsAutenticacao.Fechar()
            End If
Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
End Class
