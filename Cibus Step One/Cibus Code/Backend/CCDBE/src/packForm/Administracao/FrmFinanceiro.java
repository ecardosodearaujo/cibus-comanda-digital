/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package packForm.Administracao;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import packDatabase.DaoAcess;
import packForm.Padroes.FrmPesquisar;

/**
 *
 * @author everaldo
 */
public class FrmFinanceiro extends javax.swing.JInternalFrame {
    DaoAcess conexao = new DaoAcess();
    /**
     * Creates new form FrmFinanceiro
     */
    public FrmFinanceiro() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
     @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btnPesquisar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtDataInicio = new javax.swing.JFormattedTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtDataFim = new javax.swing.JFormattedTextField();
        jLabel4 = new javax.swing.JLabel();
        txtCodEmpresa = new javax.swing.JTextField();
        txtCodMesa = new javax.swing.JFormattedTextField();
        btnPesquisaEmpresa = new javax.swing.JButton();
        txtDscEmpresa = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtCodPedido = new javax.swing.JFormattedTextField();
        btnLimpaFiltro = new javax.swing.JButton();
        txtFormaPag = new javax.swing.JTextField();
        btnPesquisaFormaPag = new javax.swing.JButton();
        txtCodFormaPag = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        lblTotalSelecionado = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabResultados = new javax.swing.JTable();

        setClosable(true);
        setMaximizable(true);
        setResizable(true);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Filtros"));

        btnPesquisar.setText("Pesquisar");
        btnPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisarActionPerformed(evt);
            }
        });

        jLabel1.setText("Cod. Pedido:");

        txtDataInicio.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter()));

        jLabel2.setText("Periodo:");

        jLabel3.setText("Até");

        txtDataFim.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter()));

        jLabel4.setText("Cod. Mesa:");

        txtCodEmpresa.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtCodEmpresaFocusLost(evt);
            }
        });

        txtCodMesa.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(java.text.NumberFormat.getIntegerInstance())));

        btnPesquisaEmpresa.setText("...");
        btnPesquisaEmpresa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisaEmpresaActionPerformed(evt);
            }
        });

        txtDscEmpresa.setEditable(false);

        jLabel5.setText("Empresa:");

        txtCodPedido.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(java.text.NumberFormat.getIntegerInstance())));

        btnLimpaFiltro.setText("Limpar Filtros");
        btnLimpaFiltro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpaFiltroActionPerformed(evt);
            }
        });

        txtFormaPag.setEditable(false);

        btnPesquisaFormaPag.setText("...");
        btnPesquisaFormaPag.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisaFormaPagActionPerformed(evt);
            }
        });

        txtCodFormaPag.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtCodFormaPagFocusLost(evt);
            }
        });

        jLabel7.setText("Forma de Pagamento:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txtCodMesa, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel4)
                    .addComponent(txtCodPedido, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(txtCodEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(btnPesquisaEmpresa)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtDscEmpresa))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(txtDataInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(3, 3, 3)
                                    .addComponent(jLabel3)
                                    .addGap(3, 3, 3)
                                    .addComponent(txtDataFim, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtCodFormaPag, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnPesquisaFormaPag)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtFormaPag, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel7))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnLimpaFiltro, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPesquisar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnPesquisar)
                            .addComponent(txtDataInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)
                            .addComponent(txtDataFim, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCodPedido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(1, 1, 1)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtCodFormaPag, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnPesquisaFormaPag)
                            .addComponent(txtFormaPag, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addGap(1, 1, 1)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtCodEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCodMesa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnPesquisaEmpresa)
                            .addComponent(txtDscEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnLimpaFiltro)))
                .addContainerGap(23, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel6.setFont(new java.awt.Font("Ubuntu", 0, 20)); // NOI18N
        jLabel6.setText("Total Selecionado:");

        lblTotalSelecionado.setFont(new java.awt.Font("Ubuntu", 1, 20)); // NOI18N
        lblTotalSelecionado.setForeground(new java.awt.Color(238, 21, 21));
        lblTotalSelecionado.setText("0.00");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblTotalSelecionado)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(lblTotalSelecionado))
                .addContainerGap(30, Short.MAX_VALUE))
        );

        tabResultados.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        tabResultados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tabResultados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabResultadosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tabResultados);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 945, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 274, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisarActionPerformed
        try{
            StringBuilder sSqlComand = new StringBuilder();
            sSqlComand.append("SELECT");
            sSqlComand.append(" TAB_FINANCEIRO.CODPEDIDO AS 'Cod. Pedido',\n");
            sSqlComand.append(" TAB_FINANCEIRO.CODMESA AS 'Cod. Mesa',\n");
            sSqlComand.append(" TAB_FINANCEIRO.CODEMPRESA AS 'Cod. Empresa',\n");
            sSqlComand.append(" TAB_FINANCEIRO.EMPRESA AS 'Empresa',\n");
            sSqlComand.append(" TAB_FINANCEIRO.CODFORMAPAGAMENTOPAI AS 'Cod. Forma Pagamento',\n");
            sSqlComand.append(" TAB_FINANCEIRO.FORMAPAGAMENTO AS 'Forma Pagamento',\n");
            sSqlComand.append(" TAB_FINANCEIRO.QUANTPARCELAS AS 'Quant. Parcelas',\n");
            sSqlComand.append(" TAB_FINANCEIRO.VALORTOTAL AS 'Valor Total',\n");
            sSqlComand.append(" TAB_FINANCEIRO.DATACRIACAO AS 'Data Criação',\n");
            sSqlComand.append(" TAB_FINANCEIRO.DATAFECHAMENTO AS 'Data Fechamento'\n");
            sSqlComand.append("FROM TAB_FINANCEIRO\n");
            sSqlComand.append("INNER JOIN TAB_PEDIDOS ON TAB_PEDIDOS.CODPEDIDO=TAB_FINANCEIRO.CODPEDIDO\n");
            sSqlComand.append("WHERE TAB_PEDIDOS.STATUS=0\n");   
            boolean entrou=true;
            if (!txtCodPedido.getText().equals("")){
                if (entrou==true){
                    sSqlComand.append("AND\n");
                    entrou=false;
                }
                sSqlComand.append("TAB_FINANCEIRO.CODPEDIDO LIKE '%"+txtCodPedido.getText()+"%'\n");
                entrou=true;
            }
            if(!txtDataInicio.getText().equals("") && !txtDataFim.getText().equals("")){ 
                if (entrou==true){
                    sSqlComand.append("AND\n");
                    entrou=false;
                }
                sSqlComand.append("TAB_FINANCEIRO.DATAFECHAMENTO BETWEEN  '"+converteData(txtDataInicio.getText())+"' AND '"+converteData(txtDataFim.getText())+"'\n");
                entrou=true;
            }
            if(!txtCodEmpresa.getText().equals("")){
                if (entrou==true){
                    sSqlComand.append("AND\n");
                    entrou=false;
                }
                sSqlComand.append("TAB_FINANCEIRO.CODEMPRESA LIKE '"+txtCodEmpresa.getText()+"'\n");
                entrou=true;
            }
            if(!txtCodMesa.getText().equals("")){
                if (entrou==true){
                    sSqlComand.append("AND\n");
                    entrou=false;
                }
                sSqlComand.append("TAB_FINANCEIRO.CODMESA LIKE '%"+txtCodMesa.getText()+"%'");
                entrou=true;
            }
            if(!txtCodFormaPag.getText().equals("")){
                if (entrou==true){
                    sSqlComand.append("AND\n");
                    entrou=false;
                }
                sSqlComand.append("TAB_FINANCEIRO.CODFORMAPAGAMENTOPAI LIKE '%"+txtCodFormaPag.getText()+"%'");
            }
            preecheTabela(sSqlComand.toString());
            lblTotalSelecionado.setText(calculaTotalSelecionado(tabResultados)+"");            
        }catch(Exception e){
            System.out.println("Erro: "+e.getMessage());
        }
    }//GEN-LAST:event_btnPesquisarActionPerformed

    public double calculaTotalSelecionado(javax.swing.JTable tabela){
        double totalItens=0.0;
        for(int cont=0;cont<tabela.getRowCount();cont++){
                totalItens=totalItens+Double.parseDouble(tabela.getValueAt(cont, 7).toString());
            }
        return totalItens;
    }
    
    private void btnPesquisaEmpresaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisaEmpresaActionPerformed
        FrmPesquisar pesquisa = new FrmPesquisar(null,true,"CODEMPRESA AS 'Código', RAZAOSOCIAL AS 'Razão Social', CNPJ AS 'CNPJ'","TAB_EMPRESAS","","");
        pesquisa.comboPesquisa.addItem("CodEmpresa");
        pesquisa.comboPesquisa.addItem("RazaoSocial");
        pesquisa.comboPesquisa.addItem("CNPJ");
        pesquisa.setVisible(true);
        if(pesquisa.dados.size()!=0){
            txtCodEmpresa.setText(pesquisa.dados.get(0));
            txtDscEmpresa.setText(pesquisa.dados.get(1));
        }
    }//GEN-LAST:event_btnPesquisaEmpresaActionPerformed

    private void txtCodEmpresaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtCodEmpresaFocusLost
        if (!txtCodEmpresa.getText().equals("")){//Diferente de vazio;
            try {
                conexao.OpenConect();//Abre o banco;
                conexao.Select("SELECT CODEMPRESA,RAZAOSOCIAL FROM TAB_EMPRESAS "
                    + "WHERE CODEMPRESA='"+txtCodEmpresa.getText()+"'");//Passa a consulta;
                conexao.rs.first();
                txtDscEmpresa.setText(conexao.rs.getString("RAZAOSOCIAL"));//Atribui ao campo texto o nome da empresa;
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Empresa não encontrada", "Atenção",0);//Caso a empresa não seja encontrada;
                txtDscEmpresa.setText("");//Limpa os campos;
                txtCodEmpresa.setText("");//Limpa os campos;
            }
            conexao.CloseConect();//Fecha o banco;
        }else{
            txtDscEmpresa.setText("");//Limpa os campos;
            txtCodEmpresa.setText("");//Limpa os campos;
        }
    }//GEN-LAST:event_txtCodEmpresaFocusLost

    private void btnLimpaFiltroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpaFiltroActionPerformed
        txtCodPedido.setText("");
        txtDataInicio.setText("");
        txtDataFim.setText("");
        txtCodMesa.setText("");
        txtCodEmpresa.setText("");
        txtDscEmpresa.setText("");
        txtCodFormaPag.setText("");
        txtFormaPag.setText("");
    }//GEN-LAST:event_btnLimpaFiltroActionPerformed

    private void btnPesquisaFormaPagActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisaFormaPagActionPerformed
        FrmPesquisar pesquisa = new FrmPesquisar(null,true,"CODFORMAPAGAMENTO AS 'Código', FORMAPAGAMENTO AS 'Forma de Pagamento'","TAB_FORMASPAGAMENTO","","");
        pesquisa.comboPesquisa.addItem("CodFormaPagamento");
        pesquisa.comboPesquisa.addItem("FormaPagamento");
        pesquisa.setVisible(true);
        if(pesquisa.dados.size()!=0){
            txtCodFormaPag.setText(pesquisa.dados.get(0));
            txtFormaPag.setText(pesquisa.dados.get(2));
        }
    }//GEN-LAST:event_btnPesquisaFormaPagActionPerformed

    private void txtCodFormaPagFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtCodFormaPagFocusLost
        if (!txtCodFormaPag.getText().equals("")){//Diferente de vazio;
            try {
                conexao.OpenConect();//Abre o banco;
                conexao.Select("SELECT CODFORMAPAGAMENTO,FORMAPAGAMENTO FROM TAB_FORMASPAGAMENTO "
                    + "WHERE CODFORMAPAGAMENTO='"+txtCodFormaPag.getText()+"'");//Passa a consulta;
                conexao.rs.first();
                txtFormaPag.setText(conexao.rs.getString("FORMAPAGAMENTO"));//Atribui ao campo texto o nome da empresa;
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Forma de pagamento não encontrada", "Atenção",0);//Caso a empresa não seja encontrada;
                txtFormaPag.setText("");//Limpa os campos;
                txtCodFormaPag.setText("");//Limpa os campos;
            }
            conexao.CloseConect();//Fecha o banco;
        }else{
            txtFormaPag.setText("");//Limpa os campos;
            txtCodFormaPag.setText("");//Limpa os campos;
        }
    }//GEN-LAST:event_txtCodFormaPagFocusLost

    private void tabResultadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabResultadosMouseClicked
        int linha=tabResultados.getSelectedRow();
        FrmFinanceiroItem financeiroItem = new FrmFinanceiroItem(null, true, tabResultados.getValueAt(linha, 0)+"");
        financeiroItem.setVisible(true);
    }//GEN-LAST:event_tabResultadosMouseClicked

    public void preecheTabela(String Query) {
        Vector linhas = new Vector();
        Vector cabecalho = new Vector();
        String codigo;
        
        conexao.OpenConect();
        conexao.Select(Query);
        try {
            conexao.rs.beforeFirst();
            while(conexao.rs.next()){
                linhas.addElement(proximaLinha(conexao.rs,conexao.rsmd));
            }
            for(int i=1;i<=conexao.rsmd.getColumnCount();i++){
                cabecalho.addElement(conexao.rsmd.getColumnLabel(i));
            }
            codigo=conexao.rsmd.getColumnName(1);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.toString(), "Erro Interno:",0);
        }
        tabResultados.setModel(new javax.swing.table.DefaultTableModel(linhas, cabecalho));
        tabResultados.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        conexao.CloseConect();
    }
    
    public Vector proximaLinha(ResultSet rs, ResultSetMetaData rsmd){
        Vector linhaAtual = new Vector();
        try {
            for (int i=1;i<=rsmd.getColumnCount();++i){
                linhaAtual.addElement(rs.getString(i));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.toString(), "Erro Interno:",0);
        }
        return linhaAtual;
    }
    
    public String converteData(String data){
        data=data.replace("/", "-");
        String dataCorte=data,dataOrig=data;
        String ano, mes, dia;
        int contMes=1, contDia=1;
        String dataFim;
        
        //Achando o dia:
        while(!dataCorte.startsWith("-")){
            dataCorte=dataOrig.substring(contDia);
            System.out.println(dataCorte);
            contDia++;
        }
        dia=dataOrig.substring(0, contDia-1);
        dataOrig = dataOrig.substring(contDia);
        dataCorte = dataOrig;
        
        //Achando o Mes:
        while(!dataCorte.startsWith("-")){
            dataCorte=dataOrig.substring(contMes);
            contMes++;
        }
        mes=dataOrig.substring(0,contMes-1);
        dataOrig = dataOrig.substring(contMes);
        dataCorte = dataOrig;
        
        //Achando o Ano:
        ano=dataCorte;
        
        //Concatenando na forma correta:
        dataFim=ano+"-"+mes+"-"+dia;
        
        return dataFim;
    }                       
    
    
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnLimpaFiltro;
    private javax.swing.JButton btnPesquisaEmpresa;
    private javax.swing.JButton btnPesquisaFormaPag;
    private javax.swing.JButton btnPesquisar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblTotalSelecionado;
    private javax.swing.JTable tabResultados;
    private javax.swing.JTextField txtCodEmpresa;
    private javax.swing.JTextField txtCodFormaPag;
    private javax.swing.JFormattedTextField txtCodMesa;
    private javax.swing.JFormattedTextField txtCodPedido;
    private javax.swing.JFormattedTextField txtDataFim;
    private javax.swing.JFormattedTextField txtDataInicio;
    private javax.swing.JTextField txtDscEmpresa;
    private javax.swing.JTextField txtFormaPag;
    // End of variables declaration//GEN-END:variables
}
