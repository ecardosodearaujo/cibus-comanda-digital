package packForm.Cadastros;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import packDatabase.DaoAcess;
import packForm.Padroes.FrmPesquisar;

public class FrmCadUsuarios extends javax.swing.JInternalFrame {
    private DaoAcess conexao = new DaoAcess();//Variavel de conexão;
    private int codProduto;

    public FrmCadUsuarios() {
        initComponents();
        this.setTitle("Cadastro de Usuários");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btnSalvar = new javax.swing.JButton();
        btnNovo = new javax.swing.JButton();
        btnProcurar = new javax.swing.JButton();
        btnExcluir = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtUsuario = new javax.swing.JTextField();
        txtCodigo = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtSenha = new javax.swing.JPasswordField();
        jPanel3 = new javax.swing.JPanel();
        txtCodigoEmpresa = new javax.swing.JTextField();
        btnPesquisaEmpresa = new javax.swing.JButton();
        txtNomeEmpresa = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabUsuarioEmpresa = new javax.swing.JTable();
        btnRemover = new javax.swing.JButton();
        btnAdicionar = new javax.swing.JButton();

        setClosable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Cadastro de Usuários");
        setToolTipText("");

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnSalvar.setText("Salvar");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        btnNovo.setText("Novo");
        btnNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoActionPerformed(evt);
            }
        });

        btnProcurar.setText("Procurar");
        btnProcurar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProcurarActionPerformed(evt);
            }
        });

        btnExcluir.setText("Excluir");
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnProcurar, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSalvar)
                    .addComponent(btnNovo)
                    .addComponent(btnProcurar)
                    .addComponent(btnExcluir))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setText("Código:");

        txtCodigo.setEditable(false);

        jLabel2.setText("Nome de usuário:");

        jLabel3.setText("Senha:");

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Controle de acesso as empresas:"));

        txtCodigoEmpresa.setEditable(false);

        btnPesquisaEmpresa.setText("...");
        btnPesquisaEmpresa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisaEmpresaActionPerformed(evt);
            }
        });

        txtNomeEmpresa.setEditable(false);

        tabUsuarioEmpresa.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Cod. Empresa", "Razão Social"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tabUsuarioEmpresa);

        btnRemover.setText("-");
        btnRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoverActionPerformed(evt);
            }
        });

        btnAdicionar.setText("+");
        btnAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdicionarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(txtCodigoEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnPesquisaEmpresa)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtNomeEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, 307, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAdicionar, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnRemover, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jScrollPane1)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCodigoEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPesquisaEmpresa)
                    .addComponent(txtNomeEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRemover)
                    .addComponent(btnAdicionar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 246, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 404, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel3)
                    .addComponent(txtSenha, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(530, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtSenha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

   
    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        if(!txtCodigo.getText().equals("")){
            conexao.OpenConect();
            conexao.Delete("DELETE FROM TAB_USUARIOEMPRESA WHERE CODUSUARIO="+txtCodigo.getText());
            conexao.Delete("DELETE FROM TAB_USUARIOS WHERE CODUSUARIO="+txtCodigo.getText());
            conexao.CloseConect();
            JOptionPane.showMessageDialog(null, "Usuario excluido com sucesso!", "Atenção!", 1);
            limpaTela();        
        }else{
            JOptionPane.showMessageDialog(null, "Primeiramente escolha um usuário!", "Atenção!", 0);        
        }
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void btnProcurarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProcurarActionPerformed
        FrmPesquisar pesquisa = new FrmPesquisar(null,true,"CODUSUARIO AS 'Código', USUARIO AS 'Usuários'","TAB_USUARIOS","","");
        pesquisa.comboPesquisa.addItem("CodUsuario");
        pesquisa.comboPesquisa.addItem("Usuario");
        pesquisa.setVisible(true);
        if(pesquisa.dados.size()!=0){
            DefaultTableModel modelo = (DefaultTableModel) tabUsuarioEmpresa.getModel(); 
            int i=0;
            int contColunas=modelo.getRowCount();
            while(i<contColunas){
                modelo.removeRow(0);
                i++;
            }
            carregaUsuario(pesquisa.dados.get(0),pesquisa.dados.get(1),pesquisa.dados.get(2));
        }
        try {  
            conexao.OpenConect();
            conexao.Select("SELECT TAB_EMPRESAS.CODEMPRESA,TAB_EMPRESAS.RAZAOSOCIAL FROM TAB_EMPRESAS\n" +
                            "INNER JOIN TAB_USUARIOEMPRESA ON (TAB_EMPRESAS.CODEMPRESA=TAB_USUARIOEMPRESA.CODEMPRESA)\n" +
                            "WHERE CODUSUARIO="+pesquisa.dados.get(0));
            conexao.rs.beforeFirst();
            DefaultTableModel modelo = (DefaultTableModel) tabUsuarioEmpresa.getModel();
            while(conexao.rs.next()){
                modelo.addRow(new String [] {conexao.rs.getString("TAB_EMPRESAS.CODEMPRESA"),conexao.rs.getString("TAB_EMPRESAS.RAZAOSOCIAL")});
            }
        } catch (SQLException ex) {
            System.out.print("Correu algum erro: "+ex.getMessage());
        }
    }//GEN-LAST:event_btnProcurarActionPerformed

    private void btnPesquisaEmpresaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisaEmpresaActionPerformed
        FrmPesquisar pesquisa = new FrmPesquisar(null,true,"CODEMPRESA AS 'Código', RAZAOSOCIAL AS 'Razão Social', CNPJ AS 'CNPJ'","TAB_EMPRESAS","","");
        pesquisa.comboPesquisa.addItem("CodEmpresa");
        pesquisa.comboPesquisa.addItem("RazaoSocial");
        pesquisa.comboPesquisa.addItem("CNPJ");
        pesquisa.setVisible(true);
        if(pesquisa.dados.size()!=0){
            carregaEmpresa(pesquisa.dados.get(0),pesquisa.dados.get(1));
        }
    }//GEN-LAST:event_btnPesquisaEmpresaActionPerformed

    private void btnNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoActionPerformed
        limpaTela();
    }//GEN-LAST:event_btnNovoActionPerformed

    private void btnAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdicionarActionPerformed
        if(!txtCodigoEmpresa.getText().equals("") && !txtNomeEmpresa.getText().equals("") && !txtCodigo.getText().equals("")){
            boolean achou=false;
            if (tabUsuarioEmpresa.getRowCount()!=0){
                for (int i=0;i<tabUsuarioEmpresa.getRowCount();i++){
                    if (tabUsuarioEmpresa.getValueAt(i, 0).equals(txtCodigoEmpresa.getText())){
                        achou=true;
                    }
                }
                if (achou==true){
                    JOptionPane.showMessageDialog(null, "Não é possivel incluir a mesma empresa duas vezes!", "Atenção!", 0);
                }
            }
            if(achou==false){
                DefaultTableModel modelo = (DefaultTableModel) tabUsuarioEmpresa.getModel();
                modelo.addRow(new String [] {txtCodigoEmpresa.getText(),txtNomeEmpresa.getText()});
                if (!(txtCodigoEmpresa.getText().equals(""))){
                    conexao.OpenConect();
                    conexao.Insert("INSERT INTO TAB_USUARIOEMPRESA(CODUSUARIO,CODEMPRESA) VALUES("+txtCodigo.getText()+","+txtCodigoEmpresa.getText()+")");
                    conexao.CloseConect(); 
                }
            }        
            txtCodigoEmpresa.setText("");
            txtNomeEmpresa.setText("");

        }else{
            JOptionPane.showMessageDialog(null, "Informe o usuário e a empresa.\nSe for um novo cadastro salve o usuário antes!", "Atenção",0);
        }
    }//GEN-LAST:event_btnAdicionarActionPerformed

    private void btnRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoverActionPerformed
        if (tabUsuarioEmpresa.getRowCount()!=0){
            if(!txtCodigo.getText().equals("")){
                conexao.OpenConect();
                int selecao,codUsuarioEmpresa=0;
                selecao=tabUsuarioEmpresa.getSelectedRow();
                conexao.Select("SELECT CODUSUARIOEMPRESA FROM TAB_USUARIOEMPRESA WHERE CODUSUARIO="+txtCodigo.getText()+" AND CODEMPRESA="+tabUsuarioEmpresa.getValueAt(selecao, 0));
                try {
                    conexao.rs.first();
                    codUsuarioEmpresa=conexao.rs.getInt("CODUSUARIOEMPRESA");
                } catch (SQLException ex) {
                    System.out.print("Erro: "+ex.getMessage());
                }
                conexao.Delete("DELETE FROM TAB_USUARIOEMPRESA WHERE CODUSUARIOEMPRESA="+codUsuarioEmpresa);
                conexao.CloseConect();
                ((DefaultTableModel) tabUsuarioEmpresa.getModel()).removeRow(tabUsuarioEmpresa.getSelectedRow());
            }else{
                JOptionPane.showMessageDialog(null, "Informe o usuário!", "Atenção",0);            
            }
        }else{
                JOptionPane.showMessageDialog(null, "Não empresas para remover!", "Atenção",0);                        
        }
    }//GEN-LAST:event_btnRemoverActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        conexao.OpenConect();
        if(txtUsuario.getText().equals("") ){
            JOptionPane.showMessageDialog(null, "Informe o usuário", "Atenção",0);                        
        }else if(txtSenha.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Informe a senha!", "Atenção",0);                        
        }else if(txtCodigo.getText().equals("") && !txtUsuario.getText().equals("") && !txtSenha.getText().equals("")){//Insert
            try{
                conexao.Select("SELECT CODUSUARIO FROM TAB_USUARIOS ORDER BY CODUSUARIO DESC LIMIT 1");
                conexao.rs.first();
                codProduto=conexao.rs.getInt("CODUSUARIO")+1;
                conexao.Insert("INSERT INTO TAB_USUARIOS(CODUSUARIO,USUARIO,SENHA) VALUES("+codProduto+",'"+txtUsuario.getText()+"','"+txtSenha.getText()+"')");
                txtCodigo.setText(codProduto+"");
                JOptionPane.showMessageDialog(null, "Usuário Cadastrado com sucesso!", "Atenção",1); 
            }catch(SQLException e){
                JOptionPane.showMessageDialog(null, "Houve um erro na hora de cadastrar o usuário: "+e.getMessage(), "Atenção",0); 
            }
        }else{//Update
            conexao.Update("UPDATE TAB_USUARIOS SET USUARIO='"+txtUsuario.getText()+"',SENHA='"+txtSenha.getText()+"' WHERE CODUSUARIO="+txtCodigo.getText());
            JOptionPane.showMessageDialog(null,"Usuário Atualizado com sucesso!" , "Atenção",1); 
        }
        conexao.CloseConect();
    }//GEN-LAST:event_btnSalvarActionPerformed

    public void carregaEmpresa(String codigo,String empresa){
        txtCodigoEmpresa.setText(codigo);
        txtNomeEmpresa.setText(empresa);
    }
    public void carregaUsuario(String codigo,String usuario,String senha){
        txtCodigo.setText(codigo);
        txtUsuario.setText(usuario);
        txtSenha.setText(senha);
    }
    
    public void limpaTela(){
        txtCodigo.setText("");
        txtUsuario.setText("");
        txtSenha.setText("");
        txtCodigoEmpresa.setText("");
        txtNomeEmpresa.setText("");
        DefaultTableModel modelo = (DefaultTableModel) tabUsuarioEmpresa.getModel(); 
        int i=0;
        int contColunas=modelo.getRowCount();
        while(i<=contColunas){
            modelo.removeRow(0);
            i++;
        }  
    }
    

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdicionar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnNovo;
    private javax.swing.JButton btnPesquisaEmpresa;
    private javax.swing.JButton btnProcurar;
    private javax.swing.JButton btnRemover;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tabUsuarioEmpresa;
    private javax.swing.JTextField txtCodigo;
    private javax.swing.JTextField txtCodigoEmpresa;
    private javax.swing.JTextField txtNomeEmpresa;
    private javax.swing.JPasswordField txtSenha;
    private javax.swing.JTextField txtUsuario;
    // End of variables declaration//GEN-END:variables
}
