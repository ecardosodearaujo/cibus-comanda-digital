package packForm.Padroes;


import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import packDatabase.DaoAcess;

//@author Everaldo C. Araújo
public class FrmPesquisar extends javax.swing.JDialog {
    //Objetos publicos:
    public DaoAcess con = new DaoAcess();//Variavel de conexão;
    public String tabela;//Variavel que carrega a tabela para as consultas;
    public Vector linhas = new Vector(); //Vetor que guarda as linhas da consulta;
    public Vector cabecalho = new Vector();//Vetor que guarda o Cabeçalho;  
    public String parametros;//String que recebe a consulta personalizada;
    public ArrayList<String> dados = new ArrayList();//Guarda os dados da consulta;
    public String codigo;//Variavel que carrega o codigo para pegar todos os dados da seleção;
    public String condicao; //passa o where da consulta;
    public String join;
    
    //Metodo Construtor:
    public FrmPesquisar(java.awt.Frame parent, boolean modal, String paramentros,String tabela,String join, String condicao) {
        super(parent, modal);
        this.tabela = tabela;//Recebe a tabela usada para fazer a consulta;
        this.parametros=paramentros;//Recebe a pesquisa personalizada;
        this.condicao=condicao;
        this.join=join;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        txtFiltro = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        btnPesquisar = new javax.swing.JButton();
        comboModoPesquisa = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        imgPesquisa = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        comboPesquisa = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabResultado = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Pesquisa");
        setModal(true);

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jPanel3.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 2, -1, 135));

        txtFiltro.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.add(txtFiltro, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 50, 600, 23));

        jLabel2.setText("Pesquisa:");
        jPanel3.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 30, -1, -1));

        btnPesquisar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnPesquisar.setText("Pesquisar");
        btnPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisarActionPerformed(evt);
            }
        });
        jPanel3.add(btnPesquisar, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 90, 100, 30));

        comboModoPesquisa.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Iniciado por", "Contendo" }));
        jPanel3.add(comboModoPesquisa, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 100, 120, -1));

        jLabel1.setText("Campo de pesquisa:");
        jPanel3.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 80, 140, -1));

        imgPesquisa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/packPictures/lupa.png"))); // NOI18N
        jPanel3.add(imgPesquisa, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 20, -1, -1));

        jLabel3.setText("Modo de Pesquisa:");
        jPanel3.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 80, -1, -1));

        jPanel3.add(comboPesquisa, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 100, 140, -1));
        jPanel3.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 40, -1, -1));

        tabResultado.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tabResultado.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabResultadoMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tabResultado);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 784, Short.MAX_VALUE)
            .addComponent(jScrollPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 282, Short.MAX_VALUE))
        );

        setSize(new java.awt.Dimension(794, 471));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    @SuppressWarnings("empty-statement")

    //Botão pesquisar:
    private void btnPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisarActionPerformed
        DefaultTableModel dtm = (DefaultTableModel) tabResultado.getModel();//Pegando o tipo de tabela;
        dtm.getDataVector().clear();//Limpando as Linhas da tabela;
        dtm.setColumnCount(0);//Limpando as colunas da tabela;
        if (txtFiltro.getText().equals("")) {//Se o filtro estiver vazio faz a pesquisa na tabela inteira;
            preecheTabela("SELECT "+parametros+" FROM "+tabela+" "+join); 
        }else if (comboModoPesquisa.getSelectedItem()=="Iniciado por"){//Faz a pesquisa trazendo resultados inciados com o valor da pesquisa;
            preecheTabela("SELECT "+parametros+" FROM " + tabela +" "+join+" WHERE " + comboPesquisa.getSelectedItem() + " LIKE '" + txtFiltro.getText() + "%'"); 
        }else if(comboModoPesquisa.getSelectedItem()=="Contendo"){//Faz a pesquisa trazendo resultados que contenham o valor da pesquisa;
            preecheTabela("SELECT "+parametros+" FROM " + tabela +" "+join+ " WHERE " + comboPesquisa.getSelectedItem() + " LIKE '%" + txtFiltro.getText() + "%'");
        }    
    }//GEN-LAST:event_btnPesquisarActionPerformed
    //Seleção da tabela:
    private void tabResultadoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabResultadoMouseClicked
        int linha=tabResultado.getSelectedRow();
        /*Metodo ocioso que pega os dados do JTable:
        for (int i=0;i<=tabResultado.getColumnCount()-1;i++){
            dados.add(tabResultado.getValueAt(linha,i).toString());
        }*/
        try {
            con.OpenConect();
            con.Select("SELECT * FROM "+tabela+" WHERE "+codigo+"="+tabResultado.getValueAt(linha, 0));
            con.rs.first();
            for (int i=1;i<=con.rsmd.getColumnCount();++i){//For que pega o resultado da linha e joga no vertor;
                dados.add(con.rs.getString(i));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.toString(), "Erro Interno:",0);//Alerta de falha;
        }
        this.dispose();
        con.CloseConect();
    }//GEN-LAST:event_tabResultadoMouseClicked

    //Metodo que Preenche a tabela:
    public void preecheTabela(String Query) {
        if(!condicao.equals("")){
            Query=Query+" "+condicao;
        }
        con.OpenConect();//Abrindo a conexão com o banco;
        con.Select(Query);//Passando a Query chamada no ato do click no btnPesquisar;
        try {
            con.rs.beforeFirst();//Reposicionando o ponteiro;
            while(con.rs.next()){//While que preeche o Vetor das linhas e passa para o metodo proxima linha;
                linhas.addElement(proximaLinha(con.rs,con.rsmd));
            }
            for(int i=1;i<=con.rsmd.getColumnCount();i++){
                cabecalho.addElement(con.rsmd.getColumnLabel(i));//Carregando os nomes das tabelas;
            }
            codigo=con.rsmd.getColumnName(1);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.toString(), "Erro Interno:",0);//Alerta de falha;
        }
        tabResultado.setModel(new javax.swing.table.DefaultTableModel(linhas, cabecalho));//Inserindo dados dentro do JTable;
        tabResultado.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);//Modo de seleção da tabela: Simples;
        con.CloseConect();//Fechando a conexão com Banco;
    }
    
    //Metodo responsavel por preencher as linhas da tabela dinamicamente:
    public Vector proximaLinha(ResultSet rs, ResultSetMetaData rsmd){//Recebe os paramentros de conexão;
        Vector linhaAtual = new Vector();//Vetor da linha Atual;
        try {
            for (int i=1;i<=rsmd.getColumnCount();++i){//For que pega o resultado da linha e joga no vertor;
                linhaAtual.addElement(rs.getString(i));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.toString(), "Erro Interno:",0);//Alerta de falha;
        }
        return linhaAtual;//Retorna um vetor com os dados da atual linha;
    }

    //Metodo de Principal:
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmPesquisar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmPesquisar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmPesquisar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmPesquisar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmPesquisar dialog = new FrmPesquisar(new javax.swing.JFrame(), true,"","",null,"");
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnPesquisar;
    public javax.swing.JComboBox comboModoPesquisa;
    public javax.swing.JComboBox comboPesquisa;
    private javax.swing.JLabel imgPesquisa;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tabResultado;
    private javax.swing.JTextField txtFiltro;
    // End of variables declaration//GEN-END:variables
}
