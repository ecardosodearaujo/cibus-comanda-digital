package packController;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Everaldo C. Araújo
 */
public class CtrTabela extends AbstractTableModel {
   private ArrayList linhas = null;
   private String[] colunas = null;
   
   //Construtor
   public CtrTabela(ArrayList lin,String[] col){
       setLinhas(lin);
       setColunas(col);
   }
   //Linhas
   public ArrayList getLinhas(){
       return linhas;
   }
   private void setLinhas(ArrayList dados){
       linhas=dados;
   }
   //Colunas
   public String[] getColunas(){
       return colunas;
   }
   private void setColunas(String[] nomes){
       colunas=nomes;
   }
   //Conta as colunas
 
   @Override
   public int getColumnCount(){
       return colunas.length;
   }
   //Conta as Linhas
 
   @Override
   public int getRowCount(){
       return linhas.size();
   }
   //Pega o nome das colunas
 
   @Override
   public String getColumnName(int numCol){
       return colunas[numCol];
   }
   
  
   public String getValueAt(int numLin, int numCol){
       String[] linha = (String[]) getLinhas().get(numLin);
           return linha[numCol];
   }
}
