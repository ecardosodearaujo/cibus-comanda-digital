package com.comandadigital.cibus.cardapio;

import android.cardosoesaquetto.comandadigital.adcdcs.R;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.comandadigital.cibus.app.CtrAcessHttp;
import com.comandadigital.cibus.app.CtrFile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by everaldo on 27/01/15.
 */
public class TaskCarregaSubGrupos extends AsyncTask<Integer,Void,List<String>> {
    private final List<String> subGrupos = new ArrayList<String>();
    private String codSubGrupo = null;
    private Boolean acesso=false;
    private FrgCardapioSubGrupos frgCardapioSubGrupos;
    private ArrayAdapter<String> adapter;

    public TaskCarregaSubGrupos(FrgCardapioSubGrupos frgCardapioSubGrupos){
        this.frgCardapioSubGrupos=frgCardapioSubGrupos;
    }

    @Override
    protected List<String> doInBackground(Integer... params) {
        final CtrFile ipServer = new CtrFile(frgCardapioSubGrupos.getActivity());
        int codGrupo = params[0];
        CtrAcessHttp conexao = new CtrAcessHttp();
        final JSONObject result1 = conexao.getDataJson("http://"+ipServer.LerArquivo().toString()+"/CCCDWS/TelaCardapio/CarregaSubGrupos/"+codGrupo);
        if(result1!=null){
            acesso=true;
            Log.i("Teste1 Json: ", result1.toString());
            try{
                JSONArray result2= result1.getJSONArray("objectCarregaSubGrupos");
                Log.i("Teste2 Json: ",result2.toString());
                for(int i=0;i!=result2.length();i++){
                    JSONObject result3=result2.getJSONObject(i);
                    Log.i("Teste3 Json: ",result3.toString());
                    String concat = result3.getString("codSubGrupo")+"-"+result3.getString("subGrupo");
                    subGrupos.add(concat);
                    Log.i("Controle Concat: ",concat);
                    if (i<1){
                        codSubGrupo=result3.getString("codSubGrupo");
                    }
                }
            }catch (JSONException e){
                Log.i("Log Erro 1: ", e.getMessage());
                try {
                    JSONObject result2 = result1.getJSONObject("objectCarregaSubGrupos");
                    String concat = result2.getString("codSubGrupo")+"-"+result2.getString("subGrupo");
                    subGrupos.add(concat);
                    codSubGrupo=result2.getString("codSubGrupo");
                } catch (JSONException e1) {
                    Log.i("Log Erro 2: ", e1.getMessage());
                }
            }
        }
        return subGrupos;
    }

    @Override
    protected void onPostExecute(List<String> subGrupos){
        if (acesso==true){
            adapter = new ArrayAdapter<String>(frgCardapioSubGrupos.getActivity(),android.R.layout.simple_list_item_1,android.R.id.text1,subGrupos);
            frgCardapioSubGrupos.setListAdapter(adapter);

            //Passando os dados para o listView dos produtos:
            Bundle args = new Bundle();
            args.putInt("position", Integer.parseInt(codSubGrupo));
            FrgCardapioProdutos frgProdutos = new FrgCardapioProdutos();
            frgProdutos.setArguments(args);

            FragmentManager fManager = frgCardapioSubGrupos.getFragmentManager();
            FragmentTransaction fTransaction = fManager.beginTransaction();
            fTransaction.replace(R.id.frlProdutos, frgProdutos);
            fTransaction.setTransition(fTransaction.TRANSIT_FRAGMENT_FADE);
            fTransaction.commit();
        }else{
            Toast.makeText(frgCardapioSubGrupos.getActivity(), "Não Foi Possivel Acessar o WebService!\nAguarde alguns segundos e tente novamente", Toast.LENGTH_SHORT).show();
        }
    }
}
