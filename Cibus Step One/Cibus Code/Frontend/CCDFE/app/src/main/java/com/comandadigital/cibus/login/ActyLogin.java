package com.comandadigital.cibus.login;

import android.app.AlertDialog;
import android.cardosoesaquetto.comandadigital.adcdcs.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import com.comandadigital.cibus.app.CtrMascaras;

public class ActyLogin extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acty_login);
        Spinner spnCodMesas = (Spinner)findViewById(R.id.spnCodMesas);
        EditText edtCNPJ = (EditText) findViewById(R.id.edtCNPJ);
        edtCNPJ.addTextChangedListener(CtrMascaras.insert("##.###.###/####-##", edtCNPJ));
        new TaskCarregaMesas(this,spnCodMesas).execute();
	}

    public void onClickBtnLogin(View V) {
        EditText edtCNPJ = (EditText) findViewById(R.id.edtCNPJ);
        EditText edtSenha = (EditText) findViewById(R.id.edtSenha);
        Spinner spnCodMesas = (Spinner) findViewById(R.id.spnCodMesas);

        if (edtCNPJ.getText().toString().equals("")) {
            new AlertDialog.Builder(ActyLogin.this).setTitle("Atenção").setMessage("Informe o CNPJ do estabelecimento").setNeutralButton("Ok", null).show();
        } else if (edtSenha.getText().toString().equals("")) {
            new AlertDialog.Builder(ActyLogin.this).setTitle("Atenção").setMessage("Informe a Senha do estabelecimento").setNeutralButton("Ok", null).show();
        } else {
            new TaskLogin(this,edtCNPJ,edtSenha,spnCodMesas).execute();
        }
    }

    public void onClickBtnCancelar(View V){
		this.finish();
	}

    public void onClickAlteraServer(View V){
        switch (V.getId()) {
            case R.id.tvAlteraServer: {
                new CtrAlteraServer(this);
            break;
            }
        }
    }
}
