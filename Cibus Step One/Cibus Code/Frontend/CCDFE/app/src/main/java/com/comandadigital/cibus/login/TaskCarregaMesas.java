package com.comandadigital.cibus.login;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import android.app.ProgressDialog;
import com.comandadigital.cibus.app.CtrAcessHttp;
import com.comandadigital.cibus.app.CtrFile;

import android.cardosoesaquetto.comandadigital.adcdcs.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by everaldo on 26/01/15.
 */
//Carrega Mesas:
public class TaskCarregaMesas extends AsyncTask<Void,Void,List<String>> {
    private Context context;
    private ProgressDialog progress;
    private Spinner spnCodMesas;
    private ArrayAdapter<String> adapter;
    private List<String> codMesas = new ArrayList<String>();


    public TaskCarregaMesas(Context context, Spinner spnCodMesas) {
        this.context = context;
        this.spnCodMesas = spnCodMesas;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress = new ProgressDialog(context);
        progress.setMessage("Aguarde um instante...");
        progress.setIndeterminate(false);
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    protected List<String> doInBackground(Void... voids) {
        CtrFile ipServer = new CtrFile(context);
        CtrAcessHttp conexao = new CtrAcessHttp();
        JSONObject result1 = conexao.getDataJson("http://" + ipServer.LerArquivo().toString() + "/CCCDWS/TelaDeLogin/CarregaMesas");
        if (result1 != null) {
            Log.i("Teste1 Json: ", result1.toString());
            try {
                JSONArray result2 = result1.getJSONArray("objectCarregaMesas");
                Log.i("Teste2 Json: ", result2.toString());
                for (int i = 0; i != result2.length(); i++) {
                    JSONObject result3 = result2.getJSONObject(i);
                    Log.i("Teste3 Json: ", result3.toString());
                    codMesas.add(result3.getString("codMesa"));
                }
            } catch (JSONException e) {
                Log.i("Log Erro 1: ", e.getMessage());
                try {
                    JSONObject result2 = result1.getJSONObject("objectCarregaMesas");
                    codMesas.add(result2.getString("codMesa"));
                } catch (JSONException e1) {
                    Log.i("Log Erro 2: ", e1.getMessage());
                }
            }
        } else {
            return null;
        }
        return codMesas;
    }

    //onPostExeculte:
    @Override
    protected void onPostExecute(List<String> codMesas) {
        progress.dismiss();
        if (codMesas != null) {
            adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, codMesas);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
            spnCodMesas.findViewById(R.id.spnCodMesas);
            spnCodMesas.setAdapter(adapter);
        } else {
            new CtrAlteraServer(context);
            Toast.makeText(context, "Não Foi Possivel Acessar o WebService!\nPossiveis causas:\n1-Endereço do WebService errado\n2-Falta de conexão com a rede\n3-WebService indisponivel", Toast.LENGTH_LONG).show();
        }
    }
}
