package com.comandadigital.cibus.pagamento;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;

import com.comandadigital.cibus.app.CtrAcessHttp;
import com.comandadigital.cibus.app.CtrFile;
import com.comandadigital.cibus.app.CtrVariaveis;
import com.comandadigital.cibus.cardapio.ActyCardapio;
import com.comandadigital.cibus.menu.ActyMenu;
import com.comandadigital.cibus.pedido.ActyNovoPedido;
import com.comandadigital.cibus.pedido.ActyPedido;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by everaldo on 18/02/15.
 */
public class TaskFinalizarConta extends AsyncTask<Void,Void,Void> {
    private ProgressDialog progress;
    private Context context;
    private int codFormaPag;
    private boolean status;
    private String erro;

    public TaskFinalizarConta(Context context){
        this.context=context;
        //this.codFormaPag=codFormaPag;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress = new ProgressDialog(context);
        progress.setMessage("Aguarde um instante...");
        progress.setIndeterminate(false);
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            CtrAcessHttp conexao = new CtrAcessHttp();
            final CtrFile ipServer = new CtrFile(context);
            int codPedido = CtrVariaveis.pedidoAberto.getCodPedido();

            final JSONObject result1 = conexao.getDataJson("http://"+ipServer.LerArquivo().toString()+"/CCCDWS/TelaPedido/LiberaPedido/"+codPedido+"/"+CtrVariaveis.codMesa+"/"+CtrVariaveis.cnpjEmpresa);
            if (result1.getString("status").toString().equals("sucesso")) {
                status=true;
            }else{
                status=false;
            }
        }catch (JSONException e) {
            erro="Erro na conversão de dados do servidor:\n"+e.getMessage();
            status=false;
        }catch (Exception e) {
            erro="Erro na comunicação com o servidor:\n"+e.getMessage();
            status=false;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void Void){
        progress.dismiss();
        if(status==true){
            new AlertDialog.Builder(context).setTitle("Cibus Comanda Digital").setMessage("Por favor dirija-se ao caixa para efetuar o pagamento.\n" +
                    "O Cibus Comanda Digital agradece a sua preferência.\n" +
                    "Volte sempre!").setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent it = new Intent(context,ActyNovoPedido.class);
                    context.startActivity(it);
                    ((ActyPedido) context).finish();
                    ((ActyMenu)CtrVariaveis.actyMenu).finish();
                    ((ActyCardapio)CtrVariaveis.actyCardapio).finish();
                }
            }).show();
        }else if(status==false){
            new AlertDialog.Builder(context).setTitle("Atenção").setMessage(erro).setNeutralButton("OK", null).show();
        }
    }
}
