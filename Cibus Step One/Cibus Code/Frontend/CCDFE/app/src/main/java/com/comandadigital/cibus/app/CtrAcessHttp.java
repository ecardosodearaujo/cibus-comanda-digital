package com.comandadigital.cibus.app;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;
import android.util.Log;


public class CtrAcessHttp {
	public JSONObject getDataJson(String url) {  
		//Imprime a URL passada por parametro: 
		Log.i("Controle de Log:",url);
		//Instancia um m�todo GET que recebe como parametro a URL:
        HttpGet httpget = new HttpGet(url);
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, 1000);
        //Instancia um m�todo HttpClient que mais tarde far� a chamada ao WebService:
  		HttpClient httpclient = new DefaultHttpClient(httpParameters); 
        try {
        	//Fazemos a chamada do WebService pelo HttpClient passando o HttpGet e colocando o resultado dentro de HttpResponse:
        	HttpResponse response = httpclient.execute(httpget);
	    	//Pegamos a entidade dessa resposta(N�o sei pra que...):
	    	HttpEntity entity = response.getEntity();
	    	//Verificamos se a entidade � nula para seguir:
	    	if (entity != null) {  
	    		InputStream instream = entity.getContent();
	    		//Passamoso intream para String:
	    		String result = toString(instream);  
	    		//Fechamos o instream:
	    		instream.close();
	    		//Covertemos a string para um objeto JSON:
	    		JSONObject object = new JSONObject(result);
	    		//Retorno um Json para a Activity que solicitou:
	    		return object; 
	    		}  
	    	//Retorno null em caso de erro ou em caso de n�o achar nada no entity:
	    	} catch (Exception e) {  
	    		Log.i("Controle de Log:", "Falha ao acessar Web service: "+e.getMessage());  
	    		return null;
    		}  
       	return null;  
    }  
	
    //Metodo toString, reponsavel por converter um instream em String:
    private String toString(InputStream is){  
	      byte[] bytes = new byte[1024];  
	      ByteArrayOutputStream baos = new ByteArrayOutputStream();  
	      int lidos;  
	      try {
			while ((lidos = is.read(bytes)) > 0){ 
			    baos.write(bytes, 0, lidos);  
			  }
		} catch (IOException e) {
			Log.i("Controle de Log:","Caiu no Exception do ToString");
		}  
      return new String(baos.toByteArray());  
   }  
}
