package com.comandadigital.cibus.menu;

import com.comandadigital.cibus.app.CtrExit;
import com.comandadigital.cibus.app.CtrVariaveis;
import android.cardosoesaquetto.comandadigital.adcdcs.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.comandadigital.cibus.cardapio.ActyCardapio;
import com.comandadigital.cibus.pedido.ActyPedido;

public class ActyMenu extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.acty_menu);
        CtrVariaveis.actyMenu=ActyMenu.this;
        TextView txtMenuEmpresa =(TextView)findViewById(R.id.txtMenuEmpresa);
        txtMenuEmpresa.setText(CtrVariaveis.nomeEmpresa);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuItem txtMesa = menu.add(0,0,0,"Mesa: "+ CtrVariaveis.codMesa);
		txtMesa.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		
		MenuItem txtPedido = menu.add(0,1,1,"Pedido: "+ CtrVariaveis.codPedido);
		txtPedido.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

		return true;
	}

    @Override
    public void onBackPressed(){
        new CtrExit(ActyMenu.this);
    }

	//Chama a tela do Cardapio:
	public void onClickCardapio(View v){
		Intent it = new Intent(this,ActyCardapio.class);
		startActivity(it);
	}
	
	//Chama a tela dos Pedidos em aberto:
	public void onClickMeusPedidos(View v){
		Intent it = new Intent(this,ActyPedido.class);
		startActivity(it);
	}
	
	//Chama o auxilio do gar�on:
	public void onClickAuxGarcon(View v){
					
	}
}
