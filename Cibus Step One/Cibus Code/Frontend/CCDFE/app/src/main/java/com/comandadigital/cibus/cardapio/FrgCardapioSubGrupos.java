package com.comandadigital.cibus.cardapio;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.cardosoesaquetto.comandadigital.adcdcs.R;
import com.comandadigital.cibus.app.CtrAcessHttp;
import com.comandadigital.cibus.app.CtrFile;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.os.AsyncTask;
import android.os.Bundle;

public class FrgCardapioSubGrupos extends ListFragment /*implements OnItemClickListener*/{
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frg_cardapio_subgrupos, container,false);
        new TaskCarregaSubGrupos(this).execute(getArguments().getInt("position"));
        return view;
	}
	
	//Metodo que esculta o clique na lista:
	public void onActivityCreated(Bundle savedInstanceState){
		super.onActivityCreated(savedInstanceState);
		getListView().setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> lsvt, View view, int pst, long id){
				//Pegando a string do item selecionado:
				Object adapter=getListView().getItemAtPosition(pst);
				
				//Instanciando e incializando as variaveis com o valor pego acima:
				String sOriginal=adapter.toString();
				String sCortada=adapter.toString();
				String sNumero=adapter.toString();
				
				//Capturando o c�digo da Linha selecionada: 
				for (int cont=1;!sCortada.startsWith("-");cont++){
					sCortada=sOriginal.substring(cont);
					sNumero=sOriginal.substring(0, cont);
				}
				
				Log.i("Log:","SubGrupo Selecionado: "+sNumero);//Log;
				
				//Passando os dados para o listView dos produtos:
				Bundle args = new Bundle();  
				args.putInt("position", Integer.parseInt(sNumero));//Colocando o codigo capiturado acima dentro do bundle que ser� passado ao proximo fragment;
				FrgCardapioProdutos frgProdutos = new FrgCardapioProdutos();//Instancia o fragment;	
				frgProdutos.setArguments(args);
				
				FragmentManager fManager = getFragmentManager();//Gerenciador dos fragments;
				FragmentTransaction fTransaction = fManager.beginTransaction();//Preparando o objeto de Transi��o;
				fTransaction.replace(R.id.frlProdutos, frgProdutos);//Exculta a instru��o de atulizar o fragment;
				fTransaction.setTransition(fTransaction.TRANSIT_FRAGMENT_FADE);
				fTransaction.commit();//Commita os resultados;
			}
		});
	}
}