package com.comandadigital.cibus.cardapio;

import android.cardosoesaquetto.comandadigital.adcdcs.R;

import com.comandadigital.cibus.produto.ActyProduto;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.content.Intent;
import android.os.Bundle;

public class FrgCardapioProdutos extends ListFragment {
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frg_cardapio_produtos, container,false);
        new TaskCarregaProdutos(this).execute(getArguments().getInt("position"));
        return view;
	}

	public void onActivityCreated(Bundle savedInstanceState){
		super.onActivityCreated(savedInstanceState);
		getListView().setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> lsvt, View view, int pst, long id){
				Object adapter=getListView().getItemAtPosition(pst);

				String sOriginal=adapter.toString();
				String sCortada=adapter.toString();
				String sNumero=adapter.toString();

				for (int cont=1;!sCortada.startsWith("-");cont++){
					sCortada=sOriginal.substring(cont);
					sNumero=sOriginal.substring(0, cont);
				}

				Bundle args = new Bundle();
				args.putInt("selecao",Integer.parseInt(sNumero));
                Intent it = new Intent(getActivity(), ActyProduto.class);
                it.putExtras(args);
                startActivity(it);
			}
		});
	}
}