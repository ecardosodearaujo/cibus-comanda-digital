package com.comandadigital.cibus.pedido;

import android.cardosoesaquetto.comandadigital.adcdcs.R;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.comandadigital.cibus.app.CtrVariaveis;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by everaldo on 14/02/15.
 */
public class FrgListaItensPedido extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frg_lista_itens_pedido, container,false);

        ArrayList<ObjPedidoProduto> pedidos;
        pedidos=CtrVariaveis.pedidoAberto.getPedidoProduto();
        ArrayList<HashMap<String, String>> listaItens= new ArrayList<HashMap<String, String>>();
        int quantItens=0;

        quantItens=pedidos.size();
        for (int cont=0;cont<pedidos.size();cont++){
            HashMap<String, String>  item= new HashMap<String, String>();
            item.put("PRODUTO",pedidos.get(cont).getCodProduto()+"-"+pedidos.get(cont).getProduto());
            item.put("QUANTIDADE",pedidos.get(cont).getQuantidade()+"");
            item.put("PRECO",pedidos.get(cont).getValor()+"");
            listaItens.add(item);
        }

        ListAdapter adapter = new SimpleAdapter(
                getActivity(), listaItens,
                R.layout.item_lista_pedido, new String[] { "PRODUTO",
                "QUANTIDADE", "PRECO" }, new int[] {
                R.id.actyPedido_NomeProduto, R.id.actyPedido_Quantidade,
                R.id.actyPedido_Preco });
        final ListView lstvItensPedido = (ListView) view.findViewById(R.id.lstvItensPedido);
        lstvItensPedido.setAdapter(adapter);

        calculaTotalPedido(pedidos);

        for(int cont=0;cont<quantItens;cont++){
            CtrVariaveis.codItemRemover.put(cont, -1);
        }

        lstvItensPedido.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View arg1, int pst, long id) {
                if (CtrVariaveis.pedidoAberto.getPedidoProduto().get(pst).getEnviado()==false){
                    try {
                        if (CtrVariaveis.codItemRemover.get(pst)==getCodString(lstvItensPedido.getItemAtPosition(pst).toString())) {
                            lstvItensPedido.getChildAt(pst).setBackgroundColor(Color.parseColor("#3E989C"));
                            CtrVariaveis.codItemRemover.put(pst,-1);
                            Log.i("Remove: ",CtrVariaveis.codItemRemover.toString());
                        }else if (CtrVariaveis.codItemRemover.get(pst)==-1){
                            lstvItensPedido.getChildAt(pst).setBackgroundColor(Color.parseColor("#A8A8A8"));
                            CtrVariaveis.codItemRemover.put(pst, getCodString(lstvItensPedido.getItemAtPosition(pst).toString()));
                            Log.i("Add: ",CtrVariaveis.codItemRemover.toString());
                        }
                    }catch (Exception e){
                        lstvItensPedido.getChildAt(pst).setBackgroundColor(Color.parseColor("#A8A8A8"));
                        CtrVariaveis.codItemRemover.put(pst, getCodString(lstvItensPedido.getItemAtPosition(pst).toString()));
                        Log.i("Add: ",CtrVariaveis.codItemRemover.toString());
                    }
                }else {
                    Toast.makeText(getActivity(),"Atenção! Item já enviado para preparo!",Toast.LENGTH_SHORT).show();
                }
            }
        });
        return view;
    }

    public void calculaTotalPedido(ArrayList<ObjPedidoProduto> produtos){
        TextView txtValorFinal = (TextView) getActivity().findViewById(R.id.actyPedido_ValorFinal);
        Double valorFinal=0.0;
        for(int cont=0;cont<produtos.size();cont++){
            valorFinal=valorFinal+produtos.get(cont).getValor();
        }
        CtrVariaveis.pedidoAberto.setValorTotal(valorFinal);
        txtValorFinal.setText(valorFinal+"");
    }

    public int getCodString(String itemSelecionado){
        String itemCortado=itemSelecionado;
        String itemAuxiliar;
        int itemCodigo;
        int corteFim=0;

        for(int cont=0;!itemCortado.startsWith("PRODUTO=");cont++){
            itemCortado=itemCortado.substring(1);
        }
        Log.i("Primeiro corte: ", itemCortado);

        for(int cont=0;!itemCortado.startsWith("=");cont++){
            itemCortado=itemCortado.substring(1);
        }
        Log.i("Segudno corte: ", itemCortado);

        itemAuxiliar=itemCortado.substring(1);

        while (!itemCortado.startsWith("-")){
            itemCortado=itemCortado.substring(1);
            corteFim++;
        }
        itemCodigo=Integer.parseInt(itemAuxiliar.substring(0,corteFim-1));
        return itemCodigo;
    }
}
