package com.comandadigital.cibus.cardapio;

import android.cardosoesaquetto.comandadigital.adcdcs.R;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public class FrgCardapioGrupos extends ListFragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frg_cardapio_grupos, container,false);
        new TaskCarregaGrupos(this).execute();
        return view;
	}

	public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getListView().setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> lsvt, View view, int pst, long id) {
                Object adapter = getListView().getItemAtPosition(pst);

                String sOriginal = adapter.toString();
                String sCortada = adapter.toString();
                String sNumero = adapter.toString();

                for (int cont = 1; !sCortada.startsWith("-"); cont++) {
                    sCortada = sOriginal.substring(cont);
                    sNumero = sOriginal.substring(0, cont);
                }

                Bundle args = new Bundle();
                args.putInt("position", Integer.parseInt(sNumero));
                FrgCardapioSubGrupos frgSubGrupos = new FrgCardapioSubGrupos();
                frgSubGrupos.setArguments(args);

                FragmentManager fManager = getFragmentManager();
                FragmentTransaction fTransaction = fManager.beginTransaction();
                fTransaction.replace(R.id.frlSubGrupo, frgSubGrupos);
                fTransaction.setTransition(fTransaction.TRANSIT_FRAGMENT_FADE);
                fTransaction.commit();
            }
        });
    }
}
