package com.comandadigital.cibus.pedido;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.cardosoesaquetto.comandadigital.adcdcs.R;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import com.comandadigital.cibus.app.CtrAcessHttp;
import com.comandadigital.cibus.app.CtrFile;
import com.comandadigital.cibus.app.CtrVariaveis;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by everaldo on 15/02/15.
 */
public class TaskEnviarPedido extends AsyncTask <Void,Void,Void>{
    private ActyPedido actyPedido;
    private ProgressDialog progress;
    private String pedidoToJson;
    private boolean status=false;
    private String erro;

    public TaskEnviarPedido(ActyPedido actyPedido){
        this.actyPedido=actyPedido;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress = new ProgressDialog(actyPedido);
        progress.setMessage("Aguarde um instante...");
        progress.setIndeterminate(false);
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            Gson gson = new Gson();
            pedidoToJson = gson.toJson(CtrVariaveis.pedidoAberto);
            CtrAcessHttp conexao = new CtrAcessHttp();
            CtrFile ipServer = new CtrFile(actyPedido);
            pedidoToJson = URLEncoder.encode(pedidoToJson, "UTF-8");
            final JSONObject result1 = conexao.getDataJson("http://" + ipServer.LerArquivo().toString() + "/CCCDWS/TelaPedido/EnviaPedido/" + pedidoToJson);
            if (result1.getString("status").toString().equals("sucesso")) {
                for(int cont=0;cont<CtrVariaveis.pedidoAberto.getPedidoProduto().size();cont++){
                    CtrVariaveis.pedidoAberto.getPedidoProduto().get(cont).setEnviado(true);
                }
                status=true;
            }else{
                status=false;
            }
        }catch (JSONException e) {
           erro="Erro na conversão de dados do servidor:\n"+e.getMessage();
            status=false;
        } catch (UnsupportedEncodingException e) {
            erro="Erro na conversão de dados GSON:\n"+e.getMessage();
            status=false;
        }catch (Exception e) {
            erro="Erro na comunicação com o servidor.\nO pagamento não foi cefetivado\nTente novamente.\n"+e.getMessage();
            status=false;
        }
        return null;
    }

        @Override
    protected void onPostExecute(Void Void){
        progress.dismiss();
        if(status==true){
            new AlertDialog.Builder(actyPedido).setTitle("Atenção").setMessage("Itens enviados com sucesso!").setNeutralButton("Ok", null).show();
        }else{
            new AlertDialog.Builder(actyPedido).setTitle("Atenção").setMessage(erro).setNeutralButton("Ok", null).show();
        }
    }
}
