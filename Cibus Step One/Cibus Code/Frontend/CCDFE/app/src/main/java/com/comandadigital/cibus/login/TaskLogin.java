package com.comandadigital.cibus.login;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.cardosoesaquetto.comandadigital.adcdcs.R;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.comandadigital.cibus.app.CtrAcessHttp;
import com.comandadigital.cibus.app.CtrFile;
import com.comandadigital.cibus.app.CtrVariaveis;
import com.comandadigital.cibus.pedido.ActyNovoPedido;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by everaldo on 27/01/15.
 */
public class TaskLogin extends AsyncTask<Void,Void,Void> {
    private Context context;
    private EditText edtCNPJ;
    private EditText edtSenha;
    private Spinner spnCodMesas;
    private String mensagem;
    private ProgressDialog progress;
    private boolean acessou=false;
    private String empresa;

    public TaskLogin(Context context,EditText edtCNPJ,EditText edtSenha, Spinner spnCodMesas) {
        this.context = context;
        this.spnCodMesas=spnCodMesas;
        this.edtSenha=edtSenha;
        this.edtCNPJ=edtCNPJ;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
       progress = new ProgressDialog(context);
       progress.setMessage("Aguarde um instante...");
       progress.setIndeterminate(false);
       progress.setCancelable(false);
       progress.show();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        CtrFile ipServer = new CtrFile(context);
        edtSenha.findViewById(R.id.edtSenha);
        edtCNPJ.findViewById(R.id.edtCNPJ);
        spnCodMesas.findViewById(R.id.spnCodMesas);
        CtrVariaveis.codMesa = spnCodMesas.getSelectedItem().toString();
        CtrAcessHttp conexao = new CtrAcessHttp();

        String CNPJ=edtCNPJ.getText().toString();
        CNPJ=CNPJ.replace(".", "");
        CNPJ=CNPJ.replace("/", "");
        CNPJ=CNPJ.replace("-", "");
        CtrVariaveis.cnpjEmpresa=CNPJ;
        final JSONObject result = conexao.getDataJson("http://" + ipServer.LerArquivo().toString() + "/CCCDWS/TelaDeLogin/Login/"+CNPJ+"/"+edtSenha.getText().toString()+"/"+spnCodMesas.getSelectedItem().toString());
        if (result != null) {
            try {
                if (result.getString("status").equals("liberado")) {
                    CtrVariaveis.nomeEmpresa=result.getString("extra");
                    Intent it = new Intent(context, ActyNovoPedido.class);
                    context.startActivity(it);
                    ((ActyLogin) context).finish();
                    acessou=true;
                } else if (result.getString("status").equals("ocupado")) {
                    mensagem="Esta mesa está em uso atualmente!\nPor favor escolha outra";
                } else if (result.getString("status").equals("falta")) {
                    mensagem="Autênticação negada!\nPor favor revise os dados informados";
                }
            } catch (JSONException e) {
                mensagem = "Erro na conversão de dados JSON: "+e.getMessage();
            }
        } else {
            mensagem="Não Foi Possivel Acessar o WebService! Possiveis causas:\n1-Endereço do WebService errado\n2-Falta de conexão com a rede\n3-WebService indisponivel";
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void Void) {
        progress.dismiss();
        if (acessou==false){
                new AlertDialog.Builder(context).setTitle("Atenção").setMessage(mensagem).setNeutralButton("OK", null).show();
        }
    }
}