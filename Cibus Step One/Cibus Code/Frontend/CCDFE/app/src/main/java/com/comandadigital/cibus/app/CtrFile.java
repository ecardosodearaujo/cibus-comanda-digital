package com.comandadigital.cibus.app;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class CtrFile {
	private Context context;
	
	//Metodo construtor:
	public CtrFile(Context context){
        this.context = context;
    }
	
	//Metodo resposavel por Criar ou Editar um Arquivo caso ele exista:
	public boolean CriaArquivo(String text){
        try {
        	//Gravando os dados no arquivo:
        	FileOutputStream out = context.openFileOutput("ipServer.txt",Context.MODE_APPEND);
        	out.write(text.getBytes());
            out.flush();
            out.close();
            return true;
        //Cai no Catch para controle:
        } catch (Exception e) {
        	Toast.makeText(context, "Não foi possivel Salvar os dados", Toast.LENGTH_SHORT).show();
            return false;
        }
    }
	
	//Metodo reponsavel por Ler o arquivo e retornar uma String:
    public String LerArquivo(){
    	//Pega o diretorio:
        File file = context.getFilesDir();
        //Concatena com o nome do arquivo:
        File textfile = new File(file + "/ipServer.txt");
        //Log.i("Caminho: ",file+"ipserver.txt");
        FileInputStream input;
		try {
			//Abre o arquivo:
			input = context.openFileInput("ipServer.txt");
			//Passando os dados do disco para a memoria:
			byte[] buffer = new byte[(int)textfile.length()];
			input.read(buffer);
			//Retornado os dados em forma de string:
			return new String(buffer);
		} catch (FileNotFoundException e) {

			Log.i("Log:","Não foi possivel encontrar o arquivo de informaçôes");
		} catch (IOException e) {
			//Log Caso haja algum outro problema:
			Log.i("Log:","Houve algum problema com o arquivo de dados");
		}
		//Retorna Null caso n�o encontre nada ou haja algum problema:
		return null;
    }
    
    //Metodo reposanvel por excluir o Arquivo em disco:
    public void ExcluirArquivo(){
    	//Pega o Diretorio:
    	File file = context.getFilesDir();
    	//Concatena o Diretorio com o nome do arquivo montando a string:
    	File textfile = new File(file+"/ipServer.txt");
    	//Deleta o arquivo na origem:
    	textfile.delete();
    	//Controle de Log avisando do sucesso da exclus�o:
    	Log.i("Log:","Arquivo de dados deletado");
    }
}

