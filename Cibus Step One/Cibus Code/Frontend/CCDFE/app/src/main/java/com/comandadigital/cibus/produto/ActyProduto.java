package com.comandadigital.cibus.produto;

import android.app.Activity;
import android.app.AlertDialog;
import android.cardosoesaquetto.comandadigital.adcdcs.R;
import com.comandadigital.cibus.app.CtrVariaveis;
import com.comandadigital.cibus.pedido.ActyPedido;
import com.comandadigital.cibus.pedido.ObjPedido;
import com.comandadigital.cibus.pedido.ObjPedidoProduto;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.view.MenuItem.OnMenuItemClickListener;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.widget.Toast;

import java.util.HashMap;

public class ActyProduto extends Activity {
    private HashMap<Integer,Integer> codIngredientes = new HashMap<Integer, Integer>();
    private HashMap<Integer,Integer> codExtras = new HashMap<Integer, Integer>();
    private int codProduto;
    private int posicaoVetor=0;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.acty_produto);

        Bundle args = getIntent().getExtras();
        codProduto=args.getInt("selecao");

        final TextView txtNomeProduto = (TextView)findViewById(R.id.txtNomeProduto);
        final TextView txtDescricaoProduto = (TextView)findViewById(R.id.txtDescricaoProduto);
        final TextView txtTempoPreparoProduto = (TextView)findViewById(R.id.txtTempoPreparoProduto);
        final TextView txtPrecoProduto = (TextView)findViewById(R.id.txtPrecoProduto);
        final ListView lstvIngredientes = (ListView)findViewById(R.id.lstvIngredientes);
        final ListView lstvExtras = (ListView)findViewById(R.id.lstvExtras);
        final TextView txtIngredientes = (TextView)findViewById(R.id.txtIngredientes);
        final TextView txtExtras = (TextView)findViewById(R.id.txtExtras);
        final EditText edtQuantItem = (EditText)findViewById(R.id.actyProduto_EdtQuantItem);
        final ImageView imagemProduto = (ImageView)findViewById(R.id.actyProduto_ImagemProduto);
        final TextView txtImagemProduto = (TextView)findViewById(R.id.ActyProduto_txtImagemProduto);

        new TaskCarregaProduto(this,txtNomeProduto,txtDescricaoProduto,txtTempoPreparoProduto,txtPrecoProduto,lstvIngredientes,lstvExtras,txtIngredientes,txtExtras,imagemProduto,txtImagemProduto).execute(codProduto);

        edtQuantItem.addTextChangedListener(
            new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
                @Override
                public void afterTextChanged(Editable s) {
                    calculaTotalItem();
                }
            }
        );

        lstvIngredientes.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View arg1, int pst, long id) {
                try {
                    if (codIngredientes.get(pst) == getCodString(lstvIngredientes.getItemAtPosition(pst).toString())) {
                        lstvIngredientes.getChildAt(pst).setBackgroundColor(Color.parseColor("#3E989C"));
                        codIngredientes.remove(pst);
                    }
                }catch (Exception e){
                    lstvIngredientes.getChildAt(pst).setBackgroundColor(Color.parseColor("#A8A8A8"));
                    codIngredientes.put(pst,getCodString(lstvIngredientes.getItemAtPosition(pst).toString()));
                }
            }
        });

        lstvExtras.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View arg1, int pst, long id) {
                if(edtQuantItem.getText().toString().equals("")) {
                    new AlertDialog.Builder(ActyProduto.this).setTitle("Atenção!").setMessage("Informe a quantidade.").setNeutralButton("OK", null).show();
                }else if(edtQuantItem.getText().toString().equals("0")) {
                    new AlertDialog.Builder(ActyProduto.this).setTitle("Atenção!").setMessage("Informe a quantidade superior a zero").setNeutralButton("OK", null).show();
                }else{
                    try {
                        if (codExtras.get(pst) == getCodString(lstvExtras.getItemAtPosition(pst).toString())) {
                            lstvExtras.getChildAt(pst).setBackgroundColor(Color.parseColor("#3E989C"));
                            codExtras.remove(pst);
                            CtrVariaveis.precoExtras.put(pst, 0.0);
                            Log.i("PrecoExtras",CtrVariaveis.precoExtras.toString());
                            calculaTotalItem();
                        }
                    } catch (Exception e) {
                        lstvExtras.getChildAt(pst).setBackgroundColor(Color.parseColor("#A8A8A8"));
                        codExtras.put(pst, getCodString(lstvExtras.getItemAtPosition(pst).toString()));
                        CtrVariaveis.precoExtras.put(pst, getValorString(lstvExtras.getItemAtPosition(pst).toString()));
                        Log.i("Preco extras", CtrVariaveis.precoExtras.toString());
                        calculaTotalItem();
                    }
                }
            }
        });
    }
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
        final ListView lstvIngredientes = (ListView)findViewById(R.id.lstvIngredientes);

        MenuItem txtMesa = menu.add(0,0,0,"Mesa: "+ CtrVariaveis.codMesa);
		txtMesa.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

		MenuItem txtPedido = menu.add(0,1,1,"Pedido: "+ CtrVariaveis.codPedido);
		txtPedido.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        MenuItem txtMinhaConta = menu.add(0,2,2,"Minha Conta");
        txtMinhaConta.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        txtMinhaConta.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent it = new Intent(ActyProduto.this,ActyPedido.class);
                startActivity(it);
                return true;
            }
        });

        MenuItem txtIncluirNoPedido = menu.add(0,3,3,"Incluir no pedido");
        txtIncluirNoPedido.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        txtIncluirNoPedido.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                final EditText edtQuantItem = (EditText)findViewById(R.id.actyProduto_EdtQuantItem);
                int quant=0;
                if(!edtQuantItem.getText().toString().equals("")){
                    quant=Integer.parseInt(edtQuantItem.getText().toString());
                }

                if (quant!=0 && !edtQuantItem.getText().toString().equals("")){
                    if (lstvIngredientes.getCount() == 0) {
                        incluirNoPedido();
                    } else {
                        if (codIngredientes.size() == 0) {
                            new AlertDialog.Builder(ActyProduto.this)
                                    .setTitle("Atenção")
                                    .setMessage("Não foi incluido nenhum ingrediente opcional deseja prosseguir?")
                                    .setPositiveButton("Sim", new OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            incluirNoPedido();
                                        }
                                    }).setNegativeButton("Não", null).show();
                        } else {
                            incluirNoPedido();
                        }
                    }
                }else{
                    new AlertDialog.Builder(ActyProduto.this).setTitle("Atenção").setMessage("Informe a quantidade superior a zero").setNeutralButton("OK", null).show();
                }
                return true;
            }
        });
		return true;
	}

    @Override
    public void onBackPressed(){
        zeraPrecoExtras();
        ActyProduto.this.finish();
    }

    public void incluirNoPedido(){
        final EditText edtQuantItem = (EditText)findViewById(R.id.actyProduto_EdtQuantItem);
        final Double quantidade = Double.parseDouble(edtQuantItem.getText().toString());
        final TextView txtPrecoProduto = (TextView)findViewById(R.id.txtPrecoProduto);
        final Double valor = Double.parseDouble(txtPrecoProduto.getText().toString());
        final TextView txtNomeProduto = (TextView)findViewById(R.id.txtNomeProduto);
        final String dscProduto = txtNomeProduto.getText().toString();
        boolean itemIncluido=false;
        boolean itemEnviado=false;

        for (int cont=0;cont<CtrVariaveis.pedidoAberto.getPedidoProduto().size();cont++) {
            if (CtrVariaveis.pedidoAberto.getPedidoProduto().get(cont).getCodProduto() == codProduto) {
                posicaoVetor=cont;
                itemIncluido = true;
                itemEnviado=CtrVariaveis.pedidoAberto.getPedidoProduto().get(cont).getEnviado();
            }
        }

        if(itemEnviado==false) {
            if (itemIncluido == true) {
                new AlertDialog.Builder(ActyProduto.this)
                        .setTitle("Atenção")
                        .setMessage("Produto já incluido no pedido. Deseja atualizar o item?")
                        .setPositiveButton("Sim", new OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                CtrVariaveis.pedidoAberto.getPedidoProduto().get(posicaoVetor).atualizaItem(codProduto, dscProduto, quantidade, valor, codIngredientes, codExtras);
                                new AlertDialog.Builder(ActyProduto.this)
                                        .setTitle("Atenção")
                                        .setMessage("Item atualizado com sucesso!")
                                        .setPositiveButton("OK", new OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                zeraPrecoExtras();
                                                ActyProduto.this.finish();
                                            }
                                        }).show();
                            }
                        }).setNegativeButton("Não", null).show();
            } else {
                ObjPedidoProduto produto = new ObjPedidoProduto(codProduto, dscProduto, quantidade, valor, codIngredientes, codExtras);
                CtrVariaveis.pedidoAberto.setPedidoProdutos(produto);
                new AlertDialog.Builder(ActyProduto.this)
                        .setTitle("Atenção")
                        .setMessage("Item incluido no pedido com sucesso!")
                        .setPositiveButton("Ok", new OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                zeraPrecoExtras();
                                ActyProduto.this.finish();
                            }
                        }).show();
            }
        }else{
            new AlertDialog.Builder(ActyProduto.this)
            .setTitle("Atenção")
            .setMessage("Este item já foi enviado para preparo. Deja adicioná-lo novamente?")
            .setPositiveButton("Sim", new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ObjPedidoProduto produto = new ObjPedidoProduto(codProduto, dscProduto, quantidade, valor, codIngredientes, codExtras);
                    CtrVariaveis.pedidoAberto.setPedidoProdutos(produto);
                    new AlertDialog.Builder(ActyProduto.this)
                            .setTitle("Atenção")
                            .setMessage("Item incluido no pedido com sucesso!")
                            .setPositiveButton("Ok", new OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    zeraPrecoExtras();
                                    ActyProduto.this.finish();
                                }
                            }).show();
                }
            }).setNegativeButton("Não", null).show();
        }
    }

    public void calculaTotalItem(){
        TextView txtPrecoProduto = (TextView) findViewById(R.id.txtPrecoProduto);
        TextView txtTempoPreparo = (TextView) findViewById(R.id.txtTempoPreparoProduto);
        Double valorFinal=0.0;
        int tempoFinal=0;
        try {
            EditText edtQuantItem = (EditText) findViewById(R.id.actyProduto_EdtQuantItem);
            int quantidade = Integer.parseInt(edtQuantItem.getText().toString());
            Double precoItem = Double.parseDouble(CtrVariaveis.valProduto);
            Double somaPrecoExtras=0.0;
            for (int cont = 0; cont < CtrVariaveis.precoExtras.size(); cont++) {
                if (CtrVariaveis.precoExtras.get(cont)!=0){
                    somaPrecoExtras = somaPrecoExtras + CtrVariaveis.precoExtras.get(cont);
                }
            }
            valorFinal = (precoItem + somaPrecoExtras) * quantidade;
            txtPrecoProduto.setText(valorFinal + "");

            tempoFinal=Integer.parseInt(CtrVariaveis.tempoPreparo)*quantidade;
            txtTempoPreparo.setText(tempoFinal + "");
        } catch (Exception e) {
            txtPrecoProduto.setText(valorFinal + "");
            txtTempoPreparo.setText(tempoFinal + "");
        }
    }

    public Double getValorString(String itemSelecionado){
        String sCortada =itemSelecionado;
        String sValor;
        while(!sCortada.startsWith("PRECO=")){
            sCortada=sCortada.substring(1);
        }
        while(!sCortada.startsWith("=")){
            sCortada=sCortada.substring(1);
        }
        sCortada=sCortada.substring(1);
        sCortada=sCortada.substring(0,sCortada.length()-1);
        sValor=sCortada;
        return Double.parseDouble(sValor);
    }

    public int getCodString(String itemSelecionado){
        String sOriginal =itemSelecionado;
        String sCortada =itemSelecionado;
        String sNumero;
        String sAuxiliar;
        int contador=0;
        for(int cont=0; !sCortada.startsWith("=");cont++){
            sCortada=sOriginal.substring(cont);
        }
        sCortada=sCortada.substring(1);
        sAuxiliar=sCortada;
        while(!sCortada.startsWith("-")){
            sCortada=sCortada.substring(1);
            contador++;
        }
        sNumero=sAuxiliar.substring(0,contador);
        return Integer.parseInt(sNumero);
    }

    public void onClickBtnSoma(View v){
        EditText edtQuantItem = (EditText) findViewById(R.id.actyProduto_EdtQuantItem);
        int quantItem = Integer.parseInt(edtQuantItem.getText().toString());
        if (quantItem!=999) {
            quantItem = quantItem + 1;
            edtQuantItem.setText(quantItem + "");
        }
    }

    public void onClickBtnSubtrair(View v){
        EditText edtQuantItem = (EditText) findViewById(R.id.actyProduto_EdtQuantItem);
        int quantItem = Integer.parseInt(edtQuantItem.getText().toString());
        if (quantItem>1) {
            quantItem = quantItem - 1;
            edtQuantItem.setText(quantItem + "");
        }
    }

    public void zeraPrecoExtras(){
        for(int cont=0;cont<CtrVariaveis.precoExtras.size();cont++) {
            CtrVariaveis.precoExtras.put(cont,0.0);
        }
    }
}
