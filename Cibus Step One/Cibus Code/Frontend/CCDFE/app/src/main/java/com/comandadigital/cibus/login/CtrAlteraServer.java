package com.comandadigital.cibus.login;

import android.content.Intent;
import android.os.Bundle;
import android.content.Context;
import com.comandadigital.cibus.app.CtrFile;

/**
 * Created by everaldo on 25/01/15.
 */
public class CtrAlteraServer {

    public CtrAlteraServer(Context context){
        CtrFile ipServer = new CtrFile(context);
        Bundle args = new Bundle();
        args.putString("selecao",ipServer.LerArquivo().toString());
        ipServer.ExcluirArquivo();
        Intent it = new Intent(context, ActyServerIp.class);
        it.putExtras(args);
        context.startActivity(it);
        ((ActyLogin) context).finish();
    }
}
