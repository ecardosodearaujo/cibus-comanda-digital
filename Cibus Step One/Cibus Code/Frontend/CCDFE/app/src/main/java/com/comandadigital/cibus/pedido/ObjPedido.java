package com.comandadigital.cibus.pedido;

import java.util.ArrayList;
/**
 * Created by everaldo on 01/02/15.
 */
public class ObjPedido {
    private int codPedido;
    private double valorTotal;
    private ArrayList<ObjPedidoProduto> pedidoProdutos = new ArrayList<ObjPedidoProduto>();


    public void setCodPedido(int codPedido){
        this.codPedido=codPedido;
    }
    public void setValorTotal(double valorTotal){
        this.valorTotal=valorTotal;
    }

    public void setPedidoProdutos(ObjPedidoProduto pedidoProdutos){
        this.pedidoProdutos.add(pedidoProdutos);
    }

    public int getCodPedido(){
        return this.codPedido;
    }
    public double getValorTotal(){
        return this.valorTotal;
    }

    public ArrayList<ObjPedidoProduto> getPedidoProduto(){
        return this.pedidoProdutos;
    }


}
