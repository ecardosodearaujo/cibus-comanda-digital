package com.comandadigital.cibus.pedido;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import com.comandadigital.cibus.app.CtrAcessHttp;

import com.comandadigital.cibus.app.CtrFile;
import com.comandadigital.cibus.app.CtrVariaveis;
import com.comandadigital.cibus.menu.ActyMenu;

/**
 * Created by everaldo on 25/01/15.
 */
public class TaskNovoPedido extends AsyncTask<Void,Void,Void> {
    private android.content.Context context;
    private String codPedido;
    private ProgressDialog progress;
    private boolean status=false;

    public TaskNovoPedido(android.content.Context context){
        this.context=context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress = new ProgressDialog(context);
        progress.setMessage("Aguarde um instante...");
        progress.setIndeterminate(false);
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    protected Void doInBackground(Void... params) {
        try{
            CtrAcessHttp conexao = new CtrAcessHttp();
            final CtrFile ipServer = new CtrFile(context);
            final JSONObject result1 = conexao.getDataJson("http://"+ipServer.LerArquivo().toString()+"/CCCDWS/TelaPedido/CriaPedido/"+ CtrVariaveis.codMesa+"/1");
            if(result1!=null){
                status=true;
                try{
                   codPedido = result1.getString("codPedido");
                }catch (JSONException e){
                    Log.i("Log Erro 1: ", e.getMessage());
                }
            }
        }catch (Exception e){
            status=false;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void Void){
        progress.dismiss();
        if (status==true) {
            CtrVariaveis.codPedido = codPedido;
            CtrVariaveis.pedidoAberto.setCodPedido(Integer.parseInt(codPedido));
            Intent it = new Intent(context, ActyMenu.class);
            context.startActivity(it);
            ((ActyNovoPedido) context).finish();
        }else{
            Toast.makeText(context,"Não foi possivel acessar o webservice!\nAguarde alguns segundos e tente novamente.",Toast.LENGTH_LONG).show();
        }
    }
}
