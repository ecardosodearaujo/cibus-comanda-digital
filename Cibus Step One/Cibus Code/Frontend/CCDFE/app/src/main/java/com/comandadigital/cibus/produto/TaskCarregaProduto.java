package com.comandadigital.cibus.produto;

import android.app.ProgressDialog;
import android.cardosoesaquetto.comandadigital.adcdcs.R;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.comandadigital.cibus.app.CtrAcessHttp;

import java.util.HashMap;
import java.util.List;

import com.comandadigital.cibus.app.CtrFile;
import com.comandadigital.cibus.app.CtrVariaveis;

import java.util.ArrayList;

/**
 * Created by everaldo on 27/01/15.
 */

public class TaskCarregaProduto extends AsyncTask<Integer,Void,Void> {
    private TextView txtNomeProduto;
    private TextView txtDescricaoProduto;
    private TextView txtTempoPreparoProduto;
    private TextView txtPrecoProduto;
    private ListView lstvIngredientes;
    private ListView lstvExtras;
    private ActyProduto actyProduto;
    private ObjCaracteristicasProduto caracteristicas = new ObjCaracteristicasProduto();
    private boolean carregaCaracteristicas=false;
    private boolean carregaIngredientes=false;
    private List<String> ingredientes = new ArrayList<String>();
    private boolean carregaExtras=false;
    private ArrayList<HashMap<String,String>> listaExtras= new ArrayList<HashMap<String,String>>();
    private TextView txtIngredientes;
    private TextView txtExtras;
    private ProgressDialog progress;
    private ImageView imgProduto;
    private TextView txtImagemProduto;
    private boolean temFoto=false;

    public TaskCarregaProduto(ActyProduto actyProduto,TextView txtNomeProduto,
                              TextView txtDescricaoProduto,TextView txtTempoPreparoProduto,
                              TextView txtPrecoProduto,ListView lstvIngredientes,
                              ListView lstvExtras,TextView txtIngredientes,
                              TextView txtExtras,ImageView imgProduto,TextView txtImagemProduto){
        this.actyProduto=actyProduto;
        this.txtNomeProduto=txtNomeProduto;
        this.txtNomeProduto.findViewById(R.id.txtNomeProduto);
        this.txtDescricaoProduto=txtDescricaoProduto;
        this.txtDescricaoProduto.findViewById(R.id.txtDescricaoProduto);
        this.txtTempoPreparoProduto=txtTempoPreparoProduto;
        this.txtTempoPreparoProduto.findViewById(R.id.txtTempoPreparoProduto);
        this.txtPrecoProduto=txtPrecoProduto;
        this.txtPrecoProduto.findViewById(R.id.txtPrecoProduto);
        this.lstvIngredientes=lstvIngredientes;
        this.lstvIngredientes.findViewById(R.id.lstvIngredientes);
        this.lstvExtras=lstvExtras;
        this.lstvExtras.findViewById(R.id.lstvExtras);
        this.txtIngredientes=txtIngredientes;
        this.txtIngredientes.findViewById(R.id.txtIngredientes);
        this.txtExtras=txtExtras;
        this.txtExtras.findViewById(R.id.txtExtras);
        this.imgProduto=imgProduto;
        this.imgProduto.findViewById(R.id.actyProduto_ImagemProduto);
        this.txtImagemProduto=txtImagemProduto;
        this.txtImagemProduto.findViewById(R.id.ActyProduto_txtImagemProduto);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress = new ProgressDialog(actyProduto);
        progress.setMessage("Aguarde um instante...");
        progress.setIndeterminate(false);
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    protected Void doInBackground(Integer... params) {
        int codProduto = params[0];
        CtrFile ipServer = new CtrFile(actyProduto);
        CtrAcessHttp conexao = new CtrAcessHttp();

        JSONObject resultCaracteristicas1 = conexao.getDataJson("http://"+ipServer.LerArquivo().toString()+"/CCCDWS/TelaDeProduto/SelecionaCaracteristicasProduto/"+codProduto);
        if(resultCaracteristicas1!=null){
            carregaCaracteristicas=true;
            try{
                JSONArray resultCaracteristicas2 = resultCaracteristicas1.getJSONArray("objectCaracteristicasProduto");
                for(int i=0;i!=resultCaracteristicas2.length();i++) {
                    JSONObject resultCaracteristicas3 = resultCaracteristicas2.getJSONObject(i);
                    caracteristicas.setProduto(resultCaracteristicas3.getString("produto"));
                    caracteristicas.setCaracteristicas(resultCaracteristicas3.getString("caracteristicas"));
                    caracteristicas.setTempoPreparo(resultCaracteristicas3.getString("tempoPreparo"));
                    caracteristicas.setPreco(resultCaracteristicas3.getString("preco"));
                    if (!resultCaracteristicas3.getString("foto").equals("null")){
                        caracteristicas.setFoto(resultCaracteristicas3.getString("foto"));
                        temFoto=true;
                    }
                }
            }catch(JSONException e){
                try {
                    JSONObject resultCaracteristicas2 = resultCaracteristicas1.getJSONObject("objectCaracteristicasProduto");
                    caracteristicas.setProduto(resultCaracteristicas2.getString("produto"));
                    caracteristicas.setCaracteristicas(resultCaracteristicas2.getString("caracteristicas"));
                    caracteristicas.setTempoPreparo(resultCaracteristicas2.getString("tempoPreparo"));
                    caracteristicas.setPreco(resultCaracteristicas2.getString("preco"));
                    if (!resultCaracteristicas2.getString("foto").equals("null")){
                        caracteristicas.setFoto(resultCaracteristicas2.getString("foto"));
                        temFoto=true;
                    }
                } catch (JSONException e1) {
                    Log.i("Controle de Log2 Caracteristicas:", e1.getMessage());
                }
            }
        }

        //Ingredientes:
        JSONObject resultIngredientes1 = conexao.getDataJson("http://"+ipServer.LerArquivo().toString()+"/CCCDWS/TelaDeProduto/SelecionaIngredientesProduto/"+codProduto);//Pegando Ingredientes;
        if(resultIngredientes1!=null){
            carregaIngredientes=true;
            try {
                JSONArray resultIngredientes2 = resultIngredientes1.getJSONArray("objectIngredientesProduto");
                for(int i=0;i!=resultIngredientes2.length();i++){
                    JSONObject resultIngredientes3=resultIngredientes2.getJSONObject(i);;
                    String concat = resultIngredientes3.getInt("codProduto")+"-"+resultIngredientes3.getString("produto");
                    ingredientes.add(concat);
                }
            } catch (JSONException e) {
                try {
                    JSONObject resultIngredientes2 = resultIngredientes1.getJSONObject("objectIngredientesProduto");
                    String concat = resultIngredientes2.getInt("codProduto")+"-"+resultIngredientes2.getString("produto");
                    ingredientes.add(concat);
                } catch (JSONException e1) {
                    Log.i("Controle de Log2 Ingredientes:", e1.getMessage());
                }
            }
        }

        //Extras:
        JSONObject resultExtras1 = conexao.getDataJson("http://"+ipServer.LerArquivo().toString()+"/CCCDWS/TelaDeProduto/SelecionaExtrasProduto/"+codProduto);//Pegando Extras;
        if (resultExtras1!=null){
            carregaExtras=true;
            try {
                JSONArray resultExtras2 = resultExtras1.getJSONArray("objectExtrasProduto");
                for(int i=0;i!=resultExtras2.length();i++){
                    JSONObject resultExtras3=resultExtras2.getJSONObject(i);
                    HashMap<String,String> itemExtras = new HashMap<String, String>();
                    itemExtras.put("PRODUTO",resultExtras3.getInt("codProduto")+"-"+resultExtras3.getString("produto"));
                    itemExtras.put("PRECO",resultExtras3.getString("preco"));;
                    listaExtras.add(itemExtras);
                }
            } catch (JSONException e) {
                try {
                    JSONObject resultExtras2 = resultExtras1.getJSONObject("objectExtrasProduto");
                    HashMap<String,String> itemExtras = new HashMap<String, String>();
                    itemExtras.put("PRODUTO",resultExtras2.getInt("codProduto")+"-"+resultExtras2.getString("produto"));
                    itemExtras.put("PRECO",resultExtras2.getString("preco"));;
                    listaExtras.add(itemExtras);
                } catch (JSONException e1) {
                    Log.i("Controle de Log2 Extras:", e1.getMessage());
                }
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void Void){
        progress.dismiss();
        //Caracterisiticas:
        if(carregaCaracteristicas==true){
            txtNomeProduto.setText(caracteristicas.getProduto());
            txtDescricaoProduto.setText(caracteristicas.getCaracteristicas());
            txtTempoPreparoProduto.setText(caracteristicas.getTempoPreparo());
            CtrVariaveis.tempoPreparo=caracteristicas.getTempoPreparo();
            txtPrecoProduto.setText(caracteristicas.getPreco());
            if (temFoto==true) {
                imgProduto.setImageBitmap(getBitmapFromString(caracteristicas.getFoto()));
            }else{
                txtImagemProduto.setText("Não há imagem para este produto");
            }
            CtrVariaveis.valProduto=caracteristicas.getPreco().toString();

            //Ingredientes:
            if(carregaIngredientes==true){
                ArrayList<HashMap<String, String>> listaIngredientes= new ArrayList<HashMap<String, String>>();
                for(int i=0; i<ingredientes.size();i++){
                    HashMap<String, String>  item= new HashMap<String, String>();
                    item.put("INGREDIENTE",ingredientes.get(i));
                    listaIngredientes.add(item);
                }
                ListAdapter adpIngredientes = new SimpleAdapter(actyProduto, listaIngredientes,
                        R.layout.item_lista_ingrediente, new String[] {"INGREDIENTE"}, new int[] {
                        R.id.actyProduto_ItemIngrediente});
                ListView lstvIngredientes = (ListView) this.lstvIngredientes.findViewById(R.id.lstvIngredientes);
                lstvIngredientes.setAdapter(adpIngredientes);
            }else{
                txtIngredientes.setText("Não há Ingredientes para este produto");
            }
            //Extras:
            if(carregaExtras==true){
                ListAdapter adpExtras = new SimpleAdapter(actyProduto, listaExtras,
                        R.layout.item_lista_extras, new String[] {"PRODUTO","PRECO"}, new int[] {
                        R.id.actyProduto_ItemExtra,R.id.actyProduto_ItemPreco});
                ListView lstvExtras = (ListView) this.lstvExtras.findViewById(R.id.lstvExtras);
                lstvExtras.setAdapter(adpExtras);
                for(int cont=0;cont<lstvExtras.getCount();cont++){
                    CtrVariaveis.precoExtras.put(cont,0.0);
                }

            }else{
                txtExtras.setText("Não há Extras para este produto");
            }
        }else{
            Toast.makeText(actyProduto, "Não Foi Possivel Acessar o WebService!\nAguarde alguns segundos e tente novamente", Toast.LENGTH_SHORT).show();
            ((ActyProduto) actyProduto).finish();
        }
    }
    private Bitmap getBitmapFromString(String imagem){
        byte[] decodedString = Base64.decode(imagem,Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString,0,decodedString.length);
        return decodedByte;
    }
}