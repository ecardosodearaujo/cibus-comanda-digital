package com.comandadigital.cibus.pagamento;

import android.app.Dialog;
import android.cardosoesaquetto.comandadigital.adcdcs.R;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class DlgRachaConta extends Dialog{
	private Double valorFinal;
    private Context context;

	public DlgRachaConta(Context context, Double valorFinal) {
		super(context);
        this.valorFinal=valorFinal;
        this.context=context;
	}

	public void onCreate(Bundle savedInstanceState){
		setTitle("Cibus: Racha Conta");
		setContentView(R.layout.dlg_racha_conta);

        final TextView txtValorFinal = (TextView)findViewById(R.id.dlgPagamento_txtValorFinal);
        final EditText edtQuantPessoas = (EditText)findViewById(R.id.dlgPagamento_edtQuantPessoas);
        final Button btnCancelar =(Button)findViewById(R.id.btnCancelar);
		final Button btnConfirmar=(Button)findViewById(R.id.btnConfirmar);

        txtValorFinal.setText(valorFinal+"");

        rachaConta(valorFinal,1);

        edtQuantPessoas.addTextChangedListener(
            new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
                @Override
                public void afterTextChanged(Editable s) {
                    if (!edtQuantPessoas.getText().toString().equals("")) {
                        rachaConta(valorFinal, Integer.parseInt(edtQuantPessoas.getText().toString()));
                    }
                }
            }
        );
		btnCancelar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
		
		btnConfirmar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
                new TaskFinalizarConta(context).execute();
                dismiss();
			}
		});
	}
    public void rachaConta(Double valorFinal,int quantPessoas){
        TextView txtTotalPessoa =(TextView)findViewById(R.id.dlgPagamento_txtTotalPessoa);
        double valorPorPessoa = valorFinal/quantPessoas;
        txtTotalPessoa.setText(valorPorPessoa+"");
    }

}
