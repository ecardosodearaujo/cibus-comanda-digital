package com.comandadigital.cibus.cardapio;


import com.comandadigital.cibus.app.CtrVariaveis;
import com.comandadigital.cibus.pedido.ActyPedido;

import android.cardosoesaquetto.comandadigital.adcdcs.R;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import android.view.Menu;
import android.view.MenuItem;

public class ActyCardapio extends FragmentActivity {
    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.acty_cardapio);
        CtrVariaveis.actyCardapio=ActyCardapio.this;
		new TaskCarregaTopCardapio(this).execute();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuItem txtMesa = menu.add(0,0,0,"Mesa: "+ CtrVariaveis.codMesa);
		txtMesa.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		
		MenuItem txtPedido = menu.add(0,1,1,"Pedido: "+ CtrVariaveis.codPedido);
		txtPedido.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        MenuItem txtMinhaConta = menu.add(0,2,2,"Minha Conta");
        txtMinhaConta.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        txtMinhaConta.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent it = new Intent(ActyCardapio.this,ActyPedido.class);
                startActivity(it);
                return true;
            }
        });
		return true;
	}
}
