package com.comandadigital.cibus.pedido;

import com.comandadigital.cibus.app.CtrVariaveis;
import com.comandadigital.cibus.pagamento.DlgRachaConta;

import android.app.AlertDialog;
import android.cardosoesaquetto.comandadigital.adcdcs.R;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

public class ActyPedido extends FragmentActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.acty_pedidos);

        FrgListaItensPedido listaItensPedido = new FrgListaItensPedido();
        FragmentManager fManager = getSupportFragmentManager();
        FragmentTransaction fTransaction = fManager.beginTransaction();
        fTransaction.replace(R.id.frlListaItensPedido, listaItensPedido);
        fTransaction.setTransition(fTransaction.TRANSIT_FRAGMENT_FADE);
        fTransaction.commit();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuItem txtMesa = menu.add(0,0,0,"Mesa: "+ CtrVariaveis.codMesa);
		txtMesa.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		
		MenuItem txtPedido = menu.add(0,1,1,"Pedido: "+ CtrVariaveis.codPedido);
		txtPedido.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        MenuItem txtExcluirItens = menu.add(0,2,2,"Excluir Itens");
        txtExcluirItens.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        txtExcluirItens.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
               @Override
               public boolean onMenuItemClick(MenuItem item) {
                   for(int cont=0; cont<CtrVariaveis.codItemRemover.size();cont++){
                       int codAtual=CtrVariaveis.codItemRemover.get(cont);
                       for(int cont1=0;cont1<CtrVariaveis.pedidoAberto.getPedidoProduto().size();cont1++) {
                           if (CtrVariaveis.pedidoAberto.getPedidoProduto().get(cont1).getCodProduto()==codAtual){
                               CtrVariaveis.pedidoAberto.getPedidoProduto().remove(cont1);
                           }
                       }
                   }
                   FrgListaItensPedido listaItensPedido = new FrgListaItensPedido();
                   FragmentManager fManager = getSupportFragmentManager();
                   FragmentTransaction fTransaction = fManager.beginTransaction();
                   fTransaction.replace(R.id.frlListaItensPedido, listaItensPedido);
                   fTransaction.setTransition(fTransaction.TRANSIT_FRAGMENT_FADE);
                   fTransaction.commit();
                   return true;
               }
           });

        MenuItem txtEnviarPedido = menu.add(0,3,3,"Enviar Pedido");
        txtEnviarPedido.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        txtEnviarPedido.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                ListView lstvItensPedido = (ListView) findViewById(R.id.lstvItensPedido);
                boolean achou=false;
                for(int cont=0;cont<CtrVariaveis.pedidoAberto.getPedidoProduto().size();cont++){
                    if(CtrVariaveis.pedidoAberto.getPedidoProduto().get(cont).getEnviado()==false){
                        achou=true;
                    }
                }
                if (lstvItensPedido.getCount()==0 || achou==false){
                    new AlertDialog.Builder(ActyPedido.this).setTitle("Atenção").setMessage("Não há itens para ser enviado!").setNeutralButton("Ok", null).show();
                }else {
                    new AlertDialog.Builder(ActyPedido.this)
                    .setTitle("Atenção")
                    .setMessage("Tem certeza que deseja enviar o pedido?")
                    .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            TextView valorFinal = (TextView) findViewById(R.id.actyPedido_ValorFinal);
                            CtrVariaveis.pedidoAberto.setValorTotal(Double.parseDouble(valorFinal.getText().toString()));
                            new TaskEnviarPedido(ActyPedido.this).execute();
                        }
                    }).setNegativeButton("Não", null).show();
                }
            return true;
            }
        });

        MenuItem txtEfetuarPagamento = menu.add(0,4,4,"Finalizar Conta");
        txtEfetuarPagamento.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        txtEfetuarPagamento.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                ListView lstvItensPedido = (ListView) findViewById(R.id.lstvItensPedido);
                boolean achou=false;
                for(int cont=0;cont<CtrVariaveis.pedidoAberto.getPedidoProduto().size();cont++){
                    if(CtrVariaveis.pedidoAberto.getPedidoProduto().get(cont).getEnviado()==false){
                        achou=true;
                    }
                }
                if(lstvItensPedido.getCount()>0) {
                    if (achou == false) {
                        TextView txtValorFinal = (TextView) findViewById(R.id.actyPedido_ValorFinal);
                        DlgRachaConta pagamento = new DlgRachaConta(ActyPedido.this, Double.parseDouble(txtValorFinal.getText().toString()));
                        pagamento.setCancelable(false);
                        pagamento.show();
                    } else {
                        new AlertDialog.Builder(ActyPedido.this).setTitle("Atenção").setMessage("Há itens que ainda não foram enviados para preparo.").setNeutralButton("Ok", null).show();
                    }
                }else{
                    new AlertDialog.Builder(ActyPedido.this).setTitle("Atenção").setMessage("Não foi incluido nenhum item no pedido.").setNeutralButton("Ok", null).show();
                }
                return true;
            }
        });
		return true;
	}

}