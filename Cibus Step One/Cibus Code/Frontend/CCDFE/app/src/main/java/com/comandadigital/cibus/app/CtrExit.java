package com.comandadigital.cibus.app;

import android.app.AlertDialog;

/**
 * Created by everaldo on 25/01/15.
 */
public class CtrExit {
    public CtrExit(android.content.Context context){
        new AlertDialog.Builder(context).setTitle("Atenção!").setMessage("Não é permitido fechar a aplicação após abrir um pedido").setNeutralButton("Ok", null).show();
    }
}
