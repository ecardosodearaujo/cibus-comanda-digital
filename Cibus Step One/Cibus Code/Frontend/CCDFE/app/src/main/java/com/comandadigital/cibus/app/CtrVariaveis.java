package com.comandadigital.cibus.app;

import android.content.Context;

import com.comandadigital.cibus.pedido.ObjPedido;

import java.util.HashMap;

public class CtrVariaveis {
	public static String codMesa;
	public static String codPedido;
    public static int quantidadeItem;
    public static String valProduto;
    public static String tempoPreparo;
    public static ObjPedido pedidoAberto;
    public static HashMap<Integer,Double> precoExtras;
    public static HashMap<Integer,Integer> codItemRemover;
    public static String nomeEmpresa;
    public static Context actyMenu;
    public static Context actyCardapio;
    public static String cnpjEmpresa;

    public static void zeraVariaveis(){
        codPedido="";
        quantidadeItem=0;
        valProduto="";
        tempoPreparo="";
        pedidoAberto = new ObjPedido();
        precoExtras = new HashMap<Integer,Double>();
        codItemRemover = new HashMap<Integer, Integer>();
    }
}
