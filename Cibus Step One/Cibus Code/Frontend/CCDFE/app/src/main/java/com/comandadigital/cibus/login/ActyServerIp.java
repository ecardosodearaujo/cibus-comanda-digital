package com.comandadigital.cibus.login;

import android.app.Activity;
import android.app.AlertDialog;
import android.cardosoesaquetto.comandadigital.adcdcs.R;
import com.comandadigital.cibus.app.CtrFile;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class ActyServerIp extends Activity {
	//Metodo reposanvel por criar a Activity:
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.acty_server_ip);
			
		/*-----------------------------------Verificando se j� tem um arquivo de conex�o:--------------------*/
		//Instaciando um objeto para edi��o do arquivo de conex�o:
		CtrFile ipServer = new CtrFile(this);
		
		//Verificando se existem algum arquivo dentro do disco rigido do Smartphone:
		if (!(ipServer.LerArquivo()==null)){
			//Caso exista a janela de Login � chamada diretamente e a atual janela � fechada:
			if (!ipServer.LerArquivo().toString().equals(null) || !ipServer.LerArquivo().toString().equals("")){
				Intent it = new Intent(this, ActyLogin.class);
				//Chamando a janela de login:
				startActivity(it);
				//Fechando a atual janela:
				this.finish();
			}
		}else{
			try{
				Bundle args = getIntent().getExtras();//Pegando os dados passados do fragment grupo;
				EditText edtIpServidor = (EditText)findViewById(R.id.edtIpServidor);
				edtIpServidor.setText(args.getString("selecao"));
			}catch(Exception e){
				Toast.makeText(ActyServerIp.this, "Não foi possivel encontrar o arquivo de dados: "+e.getMessage(), Toast.LENGTH_LONG).show();
			}
		}
	}


	/*A��es da tela:*/
	
	//Metodo resposavel por salvar o Endere�o do Servidor:
	public void onClickBtnOk(View V){
		//Link com o XML:
		EditText edtIpServidor = (EditText) findViewById(R.id.edtIpServidor);
		//Verificando se o campo est� v�zio:
		if (edtIpServidor.getText().toString().equals("")){
			//Aviso ao usu�rio caso o campo esteja v�zio:
    		new AlertDialog.Builder(ActyServerIp.this).setTitle("Atenção!").setMessage("Informe o endereço do Servidor!").setNeutralButton("Fechar", null).show();
			//Toast.makeText(this, "Favor informe o endere�o", Toast.LENGTH_SHORT).show();
		}else{
			//Instanciando um objeto do tipo ControllerFile para escrever no arquivo:
			CtrFile ipServer = new CtrFile(this);
			//Caso a escrita do arquivo seja concluida a janela de login � chamada:
			if (ipServer.CriaArquivo(edtIpServidor.getText().toString())==true){
				Intent it = new Intent(this, ActyLogin.class);
				//chamando a janela de login:
				startActivity(it);
				//Fechando a atual janela:
				this.finish();
			}
		}
	}
	
	//Metodo reposnsavel por cancelar a opera��o e fechar a aplica��o:
	public void onClickBtnCancelar(View V){
		//Fechando a aplica��o:
		this.finish();
	}
}
