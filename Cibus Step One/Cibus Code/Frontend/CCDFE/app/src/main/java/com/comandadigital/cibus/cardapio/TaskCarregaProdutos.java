package com.comandadigital.cibus.cardapio;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.comandadigital.cibus.app.CtrAcessHttp;
import com.comandadigital.cibus.app.CtrFile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by everaldo on 27/01/15.
 */
public class TaskCarregaProdutos extends AsyncTask<Integer,Void,List<String>> {
    private final List<String> produtos = new ArrayList<String>();
    private Boolean acesso=false;
    private FrgCardapioProdutos frgCardapioProdutos;
    private ArrayAdapter<String> adpter;

    public TaskCarregaProdutos(FrgCardapioProdutos frgCardapioProdutos){
        this.frgCardapioProdutos=frgCardapioProdutos;
    }

    @Override
    protected List<String> doInBackground(Integer... params) {
        final CtrFile ipServer = new CtrFile(frgCardapioProdutos.getActivity());
        int codSubGrupo = params[0];
        CtrAcessHttp conexao = new CtrAcessHttp();
        final JSONObject result1 = conexao.getDataJson("http://"+ipServer.LerArquivo().toString()+"/CCCDWS/TelaCardapio/CarregaProdutos/"+codSubGrupo);
        if(result1!=null){
            acesso=true;
            try{
                JSONArray result2= result1.getJSONArray("objectCarregaProdutos");
                for(int i=0;i!=result2.length();i++){
                    JSONObject result3=result2.getJSONObject(i);
                    String concat = result3.getString("codProduto")+"-"+result3.getString("produto");
                    produtos.add(concat);
                }
            }catch (JSONException e){
                Log.i("Log Erro 1: ", e.getMessage());
                try {
                    JSONObject result2 = result1.getJSONObject("objectCarregaProdutos");
                    String concat = result2.getString("codProduto")+"-"+result2.getString("produto");
                    produtos.add(concat);
                } catch (JSONException e1) {
                    Log.i("Log Erro 2: ", e1.getMessage());
                }
            }
        }
        return produtos;
    }

    @Override
    protected void onPostExecute(List<String> produtos){
        if(acesso=true){
            adpter = new ArrayAdapter<String>(frgCardapioProdutos.getActivity(),android.R.layout.simple_list_item_1,android.R.id.text1,produtos);
            frgCardapioProdutos.setListAdapter(adpter);
        }else{
            Toast.makeText(frgCardapioProdutos.getActivity(), "Não Foi Possivel Acessar o WebService!\nAguarde alguns segundos e tente novamente", Toast.LENGTH_SHORT).show();
        }
    }
}
