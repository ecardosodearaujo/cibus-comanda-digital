package com.comandadigital.cibus.pedido;

import com.comandadigital.cibus.app.CtrExit;
import com.comandadigital.cibus.app.CtrVariaveis;
import com.comandadigital.cibus.cardapio.ActyCardapio;

import android.app.Activity;
import android.cardosoesaquetto.comandadigital.adcdcs.R;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class ActyNovoPedido extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acty_novo_pedido);
        CtrVariaveis.zeraVariaveis();
        TextView txtEmpresa = (TextView)findViewById(R.id.txtEmpresa);
        txtEmpresa.setText(CtrVariaveis.nomeEmpresa);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuItem txtMesa = menu.add(0, 0, 0, "Mesa: " + CtrVariaveis.codMesa);
        txtMesa.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        MenuItem txtSair = menu.add(1, 1, 1, "Sair");
        txtSair.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        txtSair.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                //((ActyCardapio) ActyNovoPedido.this).finish();
                ActyNovoPedido.this.finish();
                return true;
            }
        });
        return true;
    }

    public void onClickBtnNovoPedido(View v) {
        new TaskNovoPedido(this).execute();
    }
}

