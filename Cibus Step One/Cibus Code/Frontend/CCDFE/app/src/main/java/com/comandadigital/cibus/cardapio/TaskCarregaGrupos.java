package com.comandadigital.cibus.cardapio;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.comandadigital.cibus.app.CtrAcessHttp;
import com.comandadigital.cibus.app.CtrFile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by everaldo on 27/01/15.
 */
public class TaskCarregaGrupos extends AsyncTask<Void,Void,List<String>> {
    private final List<String> grupos = new ArrayList<String>();
    private Boolean acesso=false;
    private ArrayAdapter<String> adpter;
    private FrgCardapioGrupos frgCardapioGrupos;

    public TaskCarregaGrupos(FrgCardapioGrupos frgCardapioGrupos){
        this.frgCardapioGrupos=frgCardapioGrupos;
    }

    @Override
    protected List<String> doInBackground(Void...Void) {
        final CtrFile ipServer = new CtrFile(frgCardapioGrupos.getActivity());
        CtrAcessHttp conexao = new CtrAcessHttp();
        final JSONObject result1 = conexao.getDataJson("http://"+ipServer.LerArquivo().toString()+"/CCCDWS/TelaCardapio/CarregaGrupos");
        if(result1!=null){
            acesso=true;
            try {
                JSONArray result2= result1.getJSONArray("objectCarregaGrupos");
                for(int i=0;i!=result2.length();i++){
                    JSONObject result3=result2.getJSONObject(i);
                    String concat = result3.getString("codGrupo")+"-"+result3.getString("grupo");
                    grupos.add(concat);
                }
            } catch (JSONException e) {
                try {
                    JSONObject result2 = result1.getJSONObject("objectCarregaGrupos");
                    String concat = result2.getString("codGrupo")+"-"+result2.getString("grupo");
                    grupos.add(concat);
                } catch (JSONException e1) {
                    Log.i("Log Erro 2: ", e1.getMessage());
                }
            }
        }
        return grupos;
    }

    @Override
    protected void onPostExecute(List<String> grupos){
        if (acesso=true){
            adpter = new ArrayAdapter<String>(frgCardapioGrupos.getActivity(),android.R.layout.simple_list_item_1,android.R.id.text1,grupos);
            frgCardapioGrupos.setListAdapter(adpter);
        }else{
            Toast.makeText(frgCardapioGrupos.getActivity(), "Não Foi Possivel Acessar o WebService!\nAguarde alguns segundos e tente novamente", Toast.LENGTH_SHORT).show();
        }
    }
}
