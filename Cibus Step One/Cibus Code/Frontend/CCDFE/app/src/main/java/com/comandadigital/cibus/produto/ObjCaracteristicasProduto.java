package com.comandadigital.cibus.produto;

public class ObjCaracteristicasProduto {
	private int codProduto;
	private int codSubGrupo;
	private String produto;
	private String tempoPreparo;
	private String caracteristicas;
	private String preco;
	private String foto;
	private int tipoProduto;
	
	
	//Setter's:
	public void setCodProduto(int codProduto){
		this.codProduto=codProduto;
	}
	public void setProduto(String produto){
		this.produto=produto;
	}
	public void setTempoPreparo(String tempoPreparo){
		this.tempoPreparo=tempoPreparo;
	}
	public void setCaracteristicas(String caracteristicas){
		this.caracteristicas=caracteristicas;
	}
	public void setCodSubGrupo(int codSubGrupo){
		this.codSubGrupo=codSubGrupo;
	}
	public void setPreco(String preco){
		this.preco=preco;
	}
	public void setFoto(String foto){
		this.foto=foto;
	}
	public void setTipoProduto(int tipoProduto){
		this.tipoProduto=tipoProduto;
	}
	
	//Getter's:
	public int getCodProduto(){
		return this.codProduto;
	}
	public String getProduto(){
		return this.produto;
	}
	public String getTempoPreparo(){
		return this.tempoPreparo;
	}
	public String getCaracteristicas(){
		return this.caracteristicas;
	}
	public int getCodSubGrupo(){
		return this.codSubGrupo;
	}
	public String getPreco(){
		return this.preco;
	}
	public String getFoto(){
		return this.foto;
	}
	public int getTipoProduto(){
		return this.tipoProduto;
	}
}
