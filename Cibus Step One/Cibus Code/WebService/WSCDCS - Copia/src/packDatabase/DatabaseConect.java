package packDatabase;

/*Importa��es:*/
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

//@author Everaldo C. Ara�jo;

public class DatabaseConect{
	/*Declara��o das variaveis para acesso ao banco de dados:*/
	public String driver="com.mysql.jdbc.Driver";//Driver de conex�o do banco de dados;
	public String banco="jdbc:mysql://127.0.0.1:3306/CCCDBD";//Caminho do banco de dados;
	public String usuario="root";//Usu�rio do banco de dados;
	public String senha="M1n3Rv@7";//Senha do Banco de dados;
	
	/*Declara��o de metodo da variavel connection:*/
	/*--------------------------------------------------*/
	protected Connection connection=null;//Declarando a v�rivel connection;
	public Connection getConnection(){//M�todo get da v�riavel connection; 
		return connection;
	}
	/*Declara��o de metodo da variavel statement:*/
	/*--------------------------------------------------*/
	public static Statement statement=null;//Declarando a v�riavel stametent; 
	public Statement getStatement(){//M�todo get da v�riavel stametent;
		return statement;
	}
	/*M�todo que abre a conex�o com o banco:*/
	/*--------------------------------------------------*/
	public boolean OpenConect(){
		boolean abriu=true;//Variavel de status;
	  	try{
	        Class.forName(driver);//Determinando o drive do banco de dados;
	        connection=DriverManager.getConnection(banco,usuario,senha);//String de aut�ntica��o no banco;
	        statement=connection.createStatement();//Criando o statement que ser� usado mais tarde;
	        System.out.println("Conex�o Aberta!");//Mensagem de log;
	    }catch (Exception e){
	    	System.out.println("A conex�o n�o foi aberta!");//Mensagem de log;
		   	abriu=false;
			}
		return abriu;//Retorno do metodo;
	}
	/*M�todo que Fecha a Conex�o com o Banco de dados:*/
	/*--------------------------------------------------*/
	public boolean CloseConect(){
		boolean fechou=true;//Variavel de status;
		try {
			connection.close();//Fechando a variavel que mantem a conex�o aberta;
			System.out.println("Conex�o Fechada!");//Mensagem de log;
			}catch (SQLException ex) {
				System.out.println("Falha para fechar a conex�o!");//Mesangem de log;
				fechou=false;
				}
		return fechou;//Retorno do metodo;
	}
}
