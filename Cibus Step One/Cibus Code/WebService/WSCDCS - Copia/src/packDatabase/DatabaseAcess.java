package packDatabase;

/*Importa��es:*/
import java.sql.ResultSet;
import java.sql.SQLException;
import com.mysql.jdbc.PreparedStatement;
import java.sql.CallableStatement;

//@author Everaldo C. Ara�jo;

public class DatabaseAcess extends DatabaseConect{
	public ResultSet resultSet = null;//Carrega o resultado do Select;
	
	/*M�todo para fazer Select no banco de dados:*/ 
	/*------------------------------------------------------------------------------------*/   
    public void Select(String query){ //Recebe como parametro uma String SQL;
    	//OpenConect();//Abre a conex�o com com o banco;
    	try {
    		System.out.println(query.trim());//Imprime a query para verifica��o;
            resultSet=getStatement().executeQuery(query.trim());//O resultSet recebe o resultado do select;
        } catch (SQLException ex) {
            System.out.println("A conex�o com o banco de dados falhou!");//Aviso em caso de erro;
        }
    }
	/*M�todo que faz Insert no banco de dados:*/
    /*------------------------------------------------------------------------------------*/
    public boolean Insert(String query){//Recebe uma String SQL como par�metro;
    	boolean salvou=true;
        //OpenConect();//Abre a conex�o com o banco;
    	try {
    		System.out.println(query.trim());//Imprime a query para verifica��o;
            PreparedStatement pst = (PreparedStatement) getConnection().prepareStatement(query.trim());//Prepara o Insert;
            pst.executeUpdate(query.trim());//Executando o update no banco;
        } catch (SQLException ex) {
            System.out.println("N�o foi possivel Salvar os dados!");//Alerta em caso de erro;
            salvou=false;
        }
    	//CloseConect();//Fecha a conex�o com banco;
    	return salvou;//Retorno do metodo;
    }
    /*Metodo para Excluir os dados de dados:*/
    /*------------------------------------------------------------------------------------*/
    public boolean Delete(String query){//Recebe como par�metro uma String SQL; 
        boolean deletou=true;
    	//OpenConect();//Abrindo a conex�o com o banco;
    	try { 
    		System.out.println(query.trim());//Imprime a query para verifica��o;
            getStatement().executeUpdate(query.trim());//Execulta a instru��o;                
        } catch (SQLException ex) {
        	System.out.println("N�o foi possivel Excluir os dados!");//Alerta em caso de erro;
        	deletou=false;
        }
    	//CloseConect();//Fecha a conex�o com o banco;
    	return deletou;//Retorno do metodo;
    }
    /*M�todo para Atualizar os dados de dados:*/
    /*------------------------------------------------------------------------------------*/
    public boolean Update(String query){//Recebe uma String SQL como paramentro;
      boolean atualizou=true;
      //OpenConect();//Abrindo a conex�o com banco;
      try {
    	  System.out.println(query.trim());//Imprime a query para verifica��o;
    	  getStatement().execute(query.trim());//Execultando o Update;
      } catch (SQLException ex) {
    	  System.out.println("N�o foi possivel atualizar os dados");//Alerta em caso de erro;
    	  atualizou=false;
      }
      //CloseConect();//Fecha a conex�o com o banco;
      return atualizou;//Retorno do metodo;
    }
    public void Procedure(String query){
        try {
            System.out.println(query);
            CallableStatement cs = connection.prepareCall(query);
            resultSet=cs.executeQuery();
        } catch (SQLException ex) {

        }
    }
}