package packController;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.PathParam;

import com.google.gson.Gson;

import packDatabase.DatabaseAcess;
import packObject.ObjectCarregaMesas;
import packObject.ObjectLogin;
import packObject.ObjectStatus;

@Path("/TelaDeLogin")
public class ControllerLogin extends DatabaseAcess {
	
	/*Metodo reposanvel por fazer a autentica��o do estabelecimento na mesa:*/
	@GET
	@Path("/Login/{cnpj}/{senha}/{codMesa}")
	@Produces("application/json")
	public ObjectStatus Login(@PathParam("cnpj") String cnpj,@PathParam("senha") String senha,@PathParam("codMesa") String codMesa ){
		ObjectStatus returnLogin = new ObjectStatus();
		try {
			OpenConect();
			Select("SELECT TAB_EMPRESAS.CODEMPRESA,TAB_EMPRESAS.RAZAOSOCIAL, TAB_MESAS.STATUS FROM TAB_EMPRESAS "
				+ "INNER JOIN TAB_MESAS ON (TAB_MESAS.CODEMPRESA=TAB_EMPRESAS.CODEMPRESA) "
				+ "WHERE TAB_EMPRESAS.CNPJ='"+cnpj+"' "
				+ "AND TAB_EMPRESAS.SENHA='"+senha+"' "
				+ "AND TAB_MESAS.CODMESA="+codMesa);
			
			resultSet.first();
			if (!resultSet.getString("TAB_EMPRESAS.CODEMPRESA").equals(null)){
				OpenConect();
				Update("UPDATE TAB_MESAS SET STATUS=1 WHERE CODMESA="+codMesa);
				CloseConect();
				returnLogin.setStatus("liberado");
				returnLogin.setExtra(resultSet.getString("TAB_EMPRESAS.RAZAOSOCIAL"));
			}else{
				returnLogin.setStatus("ocupado");
			}
			CloseConect();
		} catch (SQLException e) {
			System.out.print(e.getMessage());
			returnLogin.setStatus("falta");
		}
		System.out.println(returnLogin.getStatus().toString());
		return returnLogin;
	}
	
	/*Metodo reposanvel por Carregar as mesas para a Spninner da tela de login:*/
	@GET
	@Path("/CarregaMesas/")
	@Produces("application/json")
	public ArrayList<ObjectCarregaMesas> CarregaCodMesa(){
		ArrayList<ObjectCarregaMesas> codMesas = new ArrayList<ObjectCarregaMesas>();
		OpenConect();
		Select("SELECT CODMESA FROM TAB_MESAS WHERE STATUS=0");
		try {
			resultSet.beforeFirst();
			if (!resultSet.equals(null)){
				while (resultSet.next()){
					ObjectCarregaMesas mesa = new ObjectCarregaMesas();
					mesa.setCodMesa(resultSet.getString("CODMESA"));
					codMesas.add(mesa);
				}
			}
		} catch (SQLException e) {
			System.out.println("N�o foi possivel Carregar as mesas");
			e.printStackTrace();
		}
		CloseConect();
		return codMesas;
	}
}
