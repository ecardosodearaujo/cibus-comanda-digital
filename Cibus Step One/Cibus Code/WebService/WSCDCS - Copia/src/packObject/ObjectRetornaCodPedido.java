package packObject;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ObjectRetornaCodPedido {
	private String codPedido;

	public String getCodPedido() {
		return codPedido;
	}

	public void setCodPedido(String codPedido) {
		this.codPedido = codPedido;
	}	
}
