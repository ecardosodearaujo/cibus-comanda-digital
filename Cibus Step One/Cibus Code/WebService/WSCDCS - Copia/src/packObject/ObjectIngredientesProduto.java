package packObject;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ObjectIngredientesProduto {
	private int codProduto;
	private String produto;
	
	//Setter's:
	public void setCodProduto(int codProduto){
		this.codProduto=codProduto;
	}
	
	public void setProduto(String produto){
		this.produto=produto;
	}
	
	//Getter's:
	public int getCodProduto(){
		return this.codProduto;
	}
	
	public String getProduto(){
		return this.produto;
	}
}
