package packObject;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ObjectLogin {
    private String CNPJ;
    private String senha;
    private String codMesa;

    public void setCNPJ(String CNPJ){
        this.CNPJ=CNPJ;
    }
    public void setSenha(String senha){
        this.senha=senha;
    }
    public void setCodMesa(String codMesa){
        this.codMesa=codMesa;
    }

    public String getCNPJ(){
        return this.CNPJ;
    }
    public String getSenha(){
        return this.senha;
    }
    public String getCodMesa(){
        return this.codMesa;
    }

}
