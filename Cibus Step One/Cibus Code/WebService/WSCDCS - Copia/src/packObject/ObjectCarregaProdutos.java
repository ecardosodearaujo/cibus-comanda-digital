package packObject;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ObjectCarregaProdutos {
	private int codProduto;
	private String produto;
	
	public void setCodProduto(int codProduto){
		this.codProduto=codProduto;
	}
	public void setProduto(String produto){
		this.produto=produto;
	}
	
	public int getCodProduto(){
		return this.codProduto;
	}
	public String getProduto(){
		return this.produto;
	}
	
}
