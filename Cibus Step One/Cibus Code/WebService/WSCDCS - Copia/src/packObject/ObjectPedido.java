package packObject;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ObjectPedido {
    private int codPedido;
    private double valorTotal;
    private ArrayList<ObjectPedidoProduto> pedidoProdutos = new ArrayList<ObjectPedidoProduto>();


    public void setCodPedido(int codPedido){
        this.codPedido=codPedido;
    }
    public void setValorTotal(double valorTotal){
        this.valorTotal=valorTotal;
    }

    public void setPedidoProdutos(ObjectPedidoProduto pedidoProdutos){
        this.pedidoProdutos.add(pedidoProdutos);
    }

    public int getCodPedido(){
        return this.codPedido;
    }
    public double getValorTotal(){
        return this.valorTotal;
    }

    public ArrayList<ObjectPedidoProduto> getPedidoProduto(){
        return this.pedidoProdutos;
    }


}
