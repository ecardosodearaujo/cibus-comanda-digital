package packObject;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ObjectSelecionaProduto {
	private int codProduto;
	private int codSubGrupo;
	private String produto;
	private double tempoPreparo;
	private String caracteristicas;
	private double preco;
	private double foto;
	
	
	//Setter's:
	public void setCodProduto(int codProduto){
		this.codProduto=codProduto;
	}
	public void setProduto(String produto){
		this.produto=produto;
	}
	public void setTempoPreparo(double tempoPreparo){
		this.tempoPreparo=tempoPreparo;
	}
	public void setCaracteristicas(String caracteristicas){
		this.caracteristicas=caracteristicas;
	}
	public void setCodSubGrupo(int codSubGrupo){
		this.codSubGrupo=codSubGrupo;
	}
	public void setPreco(double preco){
		this.preco=preco;
	}
	public void setFoto(double foto){
		this.foto=foto;
	}
	
	//Getter's:
	public int getCodProduto(){
		return this.codProduto;
	}
	public String getProduto(){
		return this.produto;
	}
	public Double getTempoPreparo(){
		return this.tempoPreparo;
	}
	public String getCaracteristicas(){
		return this.caracteristicas;
	}
	public int getCodSubGrupo(){
		return this.codSubGrupo;
	}
	public double getPreco(){
		return this.preco;
	}
	public double getFoto(){
		return this.foto;
	}
}
