package packObject;
import java.sql.Blob;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ObjectCaracteristicasProduto {
	private int codProduto;
	private int codSubGrupo;
	private String produto;
	private String tempoPreparo;
	private String caracteristicas;
	private Double preco;
	private String foto;
	private int tipoProduto;
	
	
	//Setter's:
	public void setCodProduto(int codProduto){
		this.codProduto=codProduto;
	}
	public void setProduto(String produto){
		this.produto=produto;
	}
	public void setTempoPreparo(String tempoPreparo){
		this.tempoPreparo=tempoPreparo;
	}
	public void setCaracteristicas(String caracteristicas){
		this.caracteristicas=caracteristicas;
	}
	public void setCodSubGrupo(int codSubGrupo){
		this.codSubGrupo=codSubGrupo;
	}
	public void setPreco(Double preco){
		this.preco=preco;
	}
	public void setFoto(String foto){
		this.foto=foto;
	}
	public void setTipoProduto(int tipoProduto){
		this.tipoProduto=tipoProduto;
	}
	
	//Getter's:
	public int getCodProduto(){
		return this.codProduto;
	}
	public String getProduto(){
		return this.produto;
	}
	public String getTempoPreparo(){
		return this.tempoPreparo;
	}
	public String getCaracteristicas(){
		return this.caracteristicas;
	}
	public int getCodSubGrupo(){
		return this.codSubGrupo;
	}
	public Double getPreco(){
		return this.preco;
	}
	public String getFoto(){
		return this.foto.toString();
	}
	public int getTipoProduto(){
		return this.tipoProduto;
	}
}
