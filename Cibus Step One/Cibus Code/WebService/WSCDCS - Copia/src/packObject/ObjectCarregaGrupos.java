package packObject;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ObjectCarregaGrupos {
	private int codGrupo;
	private String grupo;
	
	public void setCodGrupo(int codGrupo){
		this.codGrupo=codGrupo;
	}
	public void setGrupo(String grupo){
		this.grupo=grupo;
	}
	
	public int getCodGrupo(){
		return this.codGrupo;
	}
	public String getGrupo(){
		return this.grupo;
	}
}
