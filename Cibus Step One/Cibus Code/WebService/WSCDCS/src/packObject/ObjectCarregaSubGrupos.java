package packObject;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ObjectCarregaSubGrupos {
	
	//Objetos a serem tratados:
	private int codSubGrupo;
	private String subGrupo;
	private int codGrupo;
	
	//Setter's:
	public void setCodSubGrupo(int codSubGrupo){
		this.codSubGrupo=codSubGrupo;
	}
	public void setSubGrupo(String subGrupo){
		this.subGrupo=subGrupo;
	}
	public void setCodGrupo(int codGrupo){
		this.codGrupo=codGrupo;
	}
	
	//Getter's:
	public int getCodSubGrupo(){
		return this.codSubGrupo;
	}
	public String getSubGrupo(){
		return this.subGrupo;
	}
	public int getCodGrupo(){
		return this.codGrupo;
	}
}