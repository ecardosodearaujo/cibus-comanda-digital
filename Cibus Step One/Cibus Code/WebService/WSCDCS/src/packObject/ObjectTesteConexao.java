package packObject;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ObjectTesteConexao {
	private Boolean TesteConexao;

	public Boolean getTesteConexao() {
		return TesteConexao;
	}

	public void setTesteConexao(Boolean TesteConexao) {
		this.TesteConexao = TesteConexao;
	}
}