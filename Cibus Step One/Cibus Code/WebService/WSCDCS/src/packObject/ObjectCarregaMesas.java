package packObject;

import javax.xml.bind.annotation.XmlRootElement;

/*GETER's e SETTER's do objeto Para Carregar os codigos das mesas*/
@XmlRootElement
public class ObjectCarregaMesas {
	
	//Retorno da mesa:
	private String codMesa;
	public String getCodMesa(){
		return this.codMesa;
	}
	public void setCodMesa(String codMesa){
		this.codMesa=codMesa;
	}
}
