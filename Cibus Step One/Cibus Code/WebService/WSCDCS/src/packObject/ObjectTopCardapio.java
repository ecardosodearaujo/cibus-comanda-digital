package packObject;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ObjectTopCardapio {
	private int codGrupo;
	private int codSubGrupo;
	private int codProduto;
	
	//Set:
	public void setCodProduto(int codProduto){
		this.codProduto=codProduto;
	}
	
	public void setCodSubGrupo(int codSubGrupo){
		this.codSubGrupo=codSubGrupo;
	}
	
	public void setCodGrupo(int codGrupo){
		this.codGrupo=codGrupo;
	}
	
	//Get:
	public int getCodProduto(){
		return this.codProduto;
	}
	
	public int getCodSubGrupo(){
		return this.codSubGrupo;
	}
	public int getCodGrupo(){
		return this.codGrupo;
	}
}
