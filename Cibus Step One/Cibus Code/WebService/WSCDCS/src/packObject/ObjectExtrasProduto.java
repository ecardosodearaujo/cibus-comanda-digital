package packObject;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ObjectExtrasProduto {
	private int codProduto;
	private String produto;
	private double preco;
	
	//Setter's:
	public void setCodProduto(int codProduto){
		this.codProduto=codProduto;
	}
	
	public void setProduto(String produto){
		this.produto=produto;
	}
	
	public void setPreco(Double preco){
		this.preco=preco;
	}
	
	//Getter's:
	public int getCodProduto(){
		return this.codProduto;
	}
	
	public String getProduto(){
		return this.produto;
	}
	
	public Double getPreco(){
		return this.preco;
	}
}
