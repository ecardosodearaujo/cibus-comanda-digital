package packObject;

import java.util.HashMap;

public class ObjectPedidoProduto {
    private int codProduto;
    private String produto;
    private double quantidade;
    private double valor;
    private HashMap<Integer, Integer> codIngredientes = new HashMap<Integer, Integer>();
    private HashMap<Integer, Integer> codExtras = new HashMap<Integer, Integer>();
    private boolean enviado=false;

    public ObjectPedidoProduto(int codProduto, String produto, double quantidade, double valor, HashMap<Integer, Integer> codIngredientes, HashMap<Integer, Integer> codExtras) {
        this.codProduto = codProduto;
        this.produto = produto;
        this.quantidade = quantidade;
        this.valor = valor;
        this.codIngredientes = codIngredientes;
        this.codExtras = codExtras;
    }
    
    public void setEnviado(boolean enviado){
        this.enviado=enviado;
    }
    public int getCodProduto() {
        return this.codProduto;
    }

    public double getQuantidade() {
        return this.quantidade;
    }

    public String getProduto() {
        return this.produto;
    }

    public double getValor() {
        return this.valor;
    }
    
    public boolean getEnviado(){
        return this.enviado;
    }
    
    public HashMap<Integer, Integer> getCodIngredientes() {
        return this.codIngredientes;
    }

    public HashMap<Integer, Integer> getCodExtras() {
        return this.codExtras;
    }


    public void atualizaItem(int codProduto, String produto, double quantidade, double valor, HashMap<Integer, Integer> codIngredientes, HashMap<Integer, Integer> codExtras){
        this.codProduto=codProduto;
        this.produto=produto;
        this.quantidade=quantidade;
        this.valor=valor;
        this.codIngredientes=codIngredientes;
        this.codExtras=codExtras;
    }
}
