package packObject;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ObjectStatus {
	private String status;
	private String extra;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}
	
}
