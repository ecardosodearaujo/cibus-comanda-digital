package packController;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.google.gson.Gson;

import packDatabase.DatabaseAcess;
import packObject.ObjectPedido;
import packObject.ObjectPedidoProduto;
import packObject.ObjectRetornaCodPedido;
import packObject.ObjectStatus;

@Path("/TelaPedido")
public class ControllerPedido extends DatabaseAcess{
	
	//Metodo para abertura do pedido:
	@GET
	@Path("/CriaPedido/{codMesa}/{status}")
	@Produces("application/json")
	public ObjectRetornaCodPedido CriaPedido(@PathParam("codMesa") String codMesa, @PathParam("status") String status){
		ObjectRetornaCodPedido codPedido = new ObjectRetornaCodPedido();
		try {
			OpenConect();
			Insert("INSERT INTO TAB_PEDIDOS(CODMESA,STATUS,DATAABERTURA) VALUES("+codMesa+","+status+",NOW());");
			Select("SELECT CODPEDIDO FROM TAB_PEDIDOS ORDER BY CODPEDIDO DESC LIMIT 1"); 
			resultSet.first();
			codPedido.setCodPedido(resultSet.getString("CODPEDIDO"));
			CloseConect();
		} catch (SQLException e) {
			System.out.println("Não foi possivel gerar o pedido");
		}
		return codPedido;
	}
	
	
	//Metodo para envio dos intens do pedido:
	@GET
	@Path("/EnviaPedido/{pedido}")
	@Produces("application/json")
	public ObjectStatus EnviaPedido(@PathParam("pedido") String pedidoJson){
		ObjectPedido pedido = new ObjectPedido();
		Gson gson = new Gson();
		ObjectStatus returnStatus = new ObjectStatus();
		ArrayList<ObjectPedidoProduto> produtos = new ArrayList<ObjectPedidoProduto>();
		returnStatus.setStatus("fracasso");
		pedido = gson.fromJson(pedidoJson,ObjectPedido.class);
		produtos=pedido.getPedidoProduto();
		try{
			OpenConect();
			for (int cont=0;cont<produtos.size();cont++){
				//Variaveis:
				double uValorTotal=pedido.getValorTotal();
				int iPedido=pedido.getCodPedido();
				int iCodProduto=produtos.get(cont).getCodProduto();
				double iQuantidade=produtos.get(cont).getQuantidade();
				double iValor=produtos.get(cont).getValor();
				int iCodPedidoProduto;
				boolean enviado = produtos.get(cont).getEnviado();
				HashMap<Integer,Integer> extras = new HashMap<Integer,Integer>();
				HashMap<Integer,Integer> ingredientes = new HashMap<Integer,Integer>();
				extras = produtos.get(cont).getCodExtras();
				ingredientes = produtos.get(cont).getCodIngredientes();
				
				//Log:
				System.out.println(extras.toString()+"//"+ingredientes.toString());
				
				if (enviado==false){
					//Update do valor total do pedido:
					Update ("UPDATE TAB_PEDIDOS SET VALORTOTAL='"+uValorTotal+"' WHERE CODPEDIDO="+iPedido);
					
					//Insert dos produtos:
					Insert("INSERT INTO TAB_PEDIDOPRODUTOS(CODPEDIDO,CODPRODUTO,QUANTIDADE,VALOR) "
					     + "VALUES("+iPedido+","+iCodProduto+","+iQuantidade+","+iValor+")");
					
					if((extras.size()>0)||(ingredientes.size()>0)){
						//Select do codigo do produto no pedido:
						Select("SELECT CODPEDIDOPRODUTOS FROM TAB_PEDIDOPRODUTOS WHERE CODPEDIDO="+iPedido+" AND CODPRODUTO="+iCodProduto);
						resultSet.first();
						iCodPedidoProduto=resultSet.getInt("CODPEDIDOPRODUTOS");
						
						//Insert dos extras de cada produto:
						int eNull=0;
						for (int cont1=0;cont1<extras.size()+eNull;cont1++){
							if (extras.get(cont1)!=null){
								Insert("INSERT INTO TAB_PEDIDOPRODUTOEXTRAS(CODPEDIDOPRODUTOS,CODEXTRA) "
										+ "VALUES("+iCodPedidoProduto+","+extras.get(cont1)+")");
							}else{
								eNull++;
							}
						}
						
						//Insert dos ingredientes de cada produto:
						int iNull=0;
						for(int cont2=0;cont2<ingredientes.size()+iNull;cont2++){
							if(ingredientes.get(cont2)!=null){
								Insert("INSERT INTO TAB_PEDIDOPRODUTOINGREDIENTES(CODPEDIDOPRODUTOS,CODINGREDIENTE) "
									     + "VALUES("+iCodPedidoProduto+","+ingredientes.get(cont2)+")");
							}else{
								iNull++;
							}
						}
					}
				}
			}
			returnStatus.setStatus("sucesso");
			CloseConect();
		}catch(SQLException e){
			System.out.println("Houve algum erro na inserção do pedido: "+e.getMessage());
			returnStatus.setStatus("fracasso");
		}
		return returnStatus;
	}
	
	//Metodo para pagar o pedido e finalizar o mesmo:
	@GET
	@Path("/LiberaPedido/{codPedido}/{codMesa}/{cnpj}")
	@Produces("application/json")
	public ObjectStatus FinalizarPedido(@PathParam("codPedido") int codPedido,@PathParam("codMesa") int codMesa,@PathParam("cnpj") String cnpj){
		ObjectStatus status = new ObjectStatus();
		//int codFinanceiro;
		try{
			OpenConect();
			Update("UPDATE TAB_PEDIDOS SET STATUS=2 WHERE CODPEDIDO="+codPedido);//LIBERA O PEDIDO;
			Update("UPDATE TAB_MESAS SET STATUS=0 WHERE CODMESA="+codMesa);//LIBERA A MESA PARA USO;
			Insert("INSERT INTO "
					+ "TAB_FINANCEIRO("
						+ "CODPEDIDO,"
						+ "CODMESA,"
						+ "VALORTOTAL,"
						+ "TIPO,"
						+ "CODEMPRESA,"
						+ "EMPRESA,"
						+ "DATACRIACAO) "
					+ "VALUES("
						+ codPedido+","
						+ codMesa+","
						+ "(SELECT VALORTOTAL FROM TAB_PEDIDOS WHERE CODPEDIDO="+codPedido+"),"
						+ "2,"
						+ "(SELECT CODEMPRESA FROM TAB_EMPRESAS WHERE CNPJ='"+cnpj+"' LIMIT 1),"
						+ "(SELECT RAZAOSOCIAL FROM TAB_EMPRESAS WHERE CNPJ='"+cnpj+"' LIMIT 1),"
			 			+ "(SELECT DATAABERTURA FROM TAB_PEDIDOS WHERE CODPEDIDO="+codPedido+"))");//GERA CABEÇALHO DO FINANCEIRO;
			status.setStatus("sucesso");
		}catch(Exception e){
			status.setStatus("fracasso");
			System.out.println("Houve algum erro no fechamento da conta: "+e.getMessage());
		}
		CloseConect();
		return status;
	}
}



















