package packController;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import packDatabase.DatabaseAcess;
import packObject.ObjectCarregaGrupos;
import packObject.ObjectCarregaProdutos;
import packObject.ObjectCarregaSubGrupos;
import packObject.ObjectTopCardapio;

@Path("/TelaCardapio")
public class ControllerCardapio extends DatabaseAcess{
	/*------------Carrega os Produtos------------*/
	@GET
	@Path("/CarregaTopCardapio/")
	@Produces("application/json")
	public ArrayList<ObjectTopCardapio> CarregaTopCardapio(){
		System.out.println("Nenhum resultado encontrado...");
		ArrayList<ObjectTopCardapio> topCardapios = new ArrayList<ObjectTopCardapio>(); 
		try {
			OpenConect();
			Select("SELECT TAB_GRUPOS.CODGRUPO,TAB_SUBGRUPOS.CODSUBGRUPO,TAB_PRODUTOS.CODPRODUTO FROM TAB_GRUPOS "
				 + "INNER JOIN TAB_SUBGRUPOS ON (TAB_SUBGRUPOS.CODGRUPO=TAB_GRUPOS.CODGRUPO) "
				 + "INNER JOIN TAB_PRODUTOS ON (TAB_PRODUTOS.CODSUBGRUPO=TAB_SUBGRUPOS.CODSUBGRUPO) "
				 + "LIMIT 1");
			resultSet.beforeFirst();
			if (!resultSet.equals(null)){	
				while(resultSet.next()){
					ObjectTopCardapio topCardapio = new ObjectTopCardapio();
					topCardapio.setCodGrupo(resultSet.getInt("TAB_GRUPOS.CODGRUPO"));
					topCardapio.setCodSubGrupo(resultSet.getInt("TAB_SUBGRUPOS.CODSUBGRUPO"));
					topCardapio.setCodProduto(resultSet.getInt("TAB_PRODUTOS.CODPRODUTO"));
					topCardapios.add(topCardapio);
				}
			}else{
				System.out.println("Nenhum resultado encontrado...");
			}
		} catch (SQLException e) {
			System.out.println("Não foi possivel acessar o banco de dados: "+e.getMessage());
		}
		CloseConect();
		return topCardapios;
	}
	
	/*---------------Carrega os Grupos de Produtos---------------*/
	@GET
	@Path("/CarregaGrupos")
	@Produces("application/json")
	public ArrayList<ObjectCarregaGrupos> CarregaGrupos(){
		ArrayList<ObjectCarregaGrupos> grupos = new ArrayList<ObjectCarregaGrupos>();
		OpenConect();
		Select("SELECT CODGRUPO, GRUPO FROM TAB_GRUPOS "
			 + "WHERE CODGRUPO IN "
						+ "(SELECT CODGRUPO FROM TAB_SUBGRUPOS "
						+ "WHERE CODSUBGRUPO IN "
									+ "(SELECT CODSUBGRUPO FROM TAB_PRODUTOS)) "
			 + "ORDER BY CODGRUPO ASC");
		try {
			resultSet.beforeFirst();
			if (!resultSet.equals(null)){
				while(resultSet.next()){
					ObjectCarregaGrupos grupo = new ObjectCarregaGrupos();
					grupo.setCodGrupo(resultSet.getInt("CODGRUPO"));
					grupo.setGrupo(resultSet.getString("GRUPO"));
					grupos.add(grupo);
				}
			}else{
				System.out.println("Não foi possivel Carregar os SubGrupos");
			}
		} catch (SQLException e) {
			System.out.println("Não foi possivel Carregar os Grupos");
			e.printStackTrace();
		}
		CloseConect();
		return grupos;
	}
	
	/*------------Carrega os SubGrupos de Produtos------------*/
	@GET
	@Path("/CarregaSubGrupos/{codGrupo}")
	@Produces("application/json")
	public ArrayList<ObjectCarregaSubGrupos> CarregaSubGrupos(@PathParam("codGrupo") String codGrupo){
		ArrayList<ObjectCarregaSubGrupos> subGrupos = new ArrayList<ObjectCarregaSubGrupos>();
		try {
			OpenConect();
			Select("SELECT CODSUBGRUPO, SUBGRUPO, CODGRUPO FROM TAB_SUBGRUPOS "
				 + "WHERE CODGRUPO="+codGrupo+" "
				 + "AND CODSUBGRUPO IN "
				 			+ "(SELECT CODSUBGRUPO FROM TAB_PRODUTOS)");
				resultSet.beforeFirst();
				if(!resultSet.equals(null)){
					while(resultSet.next()){
						ObjectCarregaSubGrupos subGrupo = new ObjectCarregaSubGrupos();
						subGrupo.setCodSubGrupo(resultSet.getInt("CODSUBGRUPO"));
						subGrupo.setSubGrupo(resultSet.getString("SUBGRUPO"));
						subGrupo.setCodGrupo(resultSet.getInt("CODGRUPO"));
						subGrupos.add(subGrupo);
					}
				}else{
					System.out.println("Não foi possivel Carregar os SubGrupos");
				}
		} catch (SQLException e) {
			System.out.println("Não foi possivel Carregar os SubGrupos"+e.getMessage());
		}
		CloseConect();
		return subGrupos;
	}
	
	/*------------Carrega os Produtos------------*/
	@GET
	@Path("/CarregaProdutos/{codSubGrupo}")
	@Produces("application/json")
	public ArrayList<ObjectCarregaProdutos> CarregaProdutos(@PathParam("codSubGrupo") String codSubGrupo){
		ArrayList<ObjectCarregaProdutos> produtos = new ArrayList<ObjectCarregaProdutos>();
		try{
			OpenConect();
			Select("SELECT CODPRODUTO, PRODUTO FROM TAB_PRODUTOS "
				+  "WHERE CODSUBGRUPO="+codSubGrupo);
			resultSet.beforeFirst();
			if (!resultSet.equals(null)){	
				while(resultSet.next()){
					ObjectCarregaProdutos produto = new ObjectCarregaProdutos();
					produto.setCodProduto(resultSet.getInt("CODPRODUTO"));
					produto.setProduto(resultSet.getString("PRODUTO"));
					produtos.add(produto);
				}
			}else{
				System.out.println("Não foi possivel Carregar os Produtos");
			}
		}catch (SQLException e){
			System.out.println("Não foi possivel Carregar os Produtos");
			e.printStackTrace();
		}
		CloseConect();
		return produtos;
	}
}













