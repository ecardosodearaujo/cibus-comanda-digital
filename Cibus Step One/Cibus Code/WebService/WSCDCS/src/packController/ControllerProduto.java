package packController;

import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import packDatabase.DatabaseAcess;
import packObject.ObjectCaracteristicasProduto;
import packObject.ObjectExtrasProduto;
import packObject.ObjectIngredientesProduto;
import packUtil.UtilBase64;

@Path("/TelaDeProduto")
public class ControllerProduto extends DatabaseAcess{
	/*------------Seleciona Caracteristicas de um produto Especifico:------------*/
	@GET
	@Path("/SelecionaCaracteristicasProduto/{codProduto}")
	@Produces("application/json")
	public ArrayList<ObjectCaracteristicasProduto> SelecionaCaracteristicasProduto(@PathParam("codProduto") String codProduto){
		ArrayList<ObjectCaracteristicasProduto> caracteristica = new ArrayList<ObjectCaracteristicasProduto>();
		try{
			OpenConect();
			Select("SELECT * FROM TAB_PRODUTOS WHERE CODPRODUTO="+codProduto);
			resultSet.beforeFirst();
			if (!resultSet.equals(null)){	
				while(resultSet.next()){
					ObjectCaracteristicasProduto cProduto = new ObjectCaracteristicasProduto();
					cProduto.setCodProduto(resultSet.getInt("CODPRODUTO"));
					cProduto.setCodSubGrupo(resultSet.getInt("CODSUBGRUPO"));
					cProduto.setProduto(resultSet.getString("PRODUTO"));
					cProduto.setTempoPreparo(resultSet.getString("TEMPOPREPARO"));
					cProduto.setCaracteristicas(resultSet.getString("CARACTERISTICAS"));
					cProduto.setPreco(resultSet.getDouble("PRECO"));
					Blob blob = resultSet.getBlob("FOTO");
					if(blob!=null){
						cProduto.setFoto(UtilBase64.encodeBytes(blob.getBytes(1, (int)blob.length())));
					}else{
						cProduto.setFoto("null");
					}
					cProduto.setTipoProduto(resultSet.getInt("TIPO"));
					caracteristica.add(cProduto);
				}
			}else{
				System.out.println("Não foi possivel Carregar os Produtos");
			}
		}catch (SQLException e){
			System.out.println("Não foi possivel Carregar os Produtos: "+e.getMessage());
		}
		CloseConect();
		return caracteristica;
	}
	
	/*------------------------Seleciona Ingredientes de um produto especifico:---------------*/
	@GET
	@Path("/SelecionaIngredientesProduto/{codProduto}")
	@Produces("application/json")
	public ArrayList<ObjectIngredientesProduto> SelecionaIngredientesProduto(@PathParam("codProduto") String codProduto){
		ArrayList<ObjectIngredientesProduto> ingredientes = new ArrayList<ObjectIngredientesProduto>();
		try {
			OpenConect();
			Select("SELECT CODPRODUTO,PRODUTO FROM TAB_PRODUTOS "
				 + "WHERE CODPRODUTO IN "
				 	+ "(SELECT CODINGREDIENTE FROM TAB_PRODUTOINGREDIENTES WHERE CODPRODUTO="+codProduto+")");
				resultSet.beforeFirst();
				while(resultSet.next()){
					ObjectIngredientesProduto iProduto = new ObjectIngredientesProduto();
					iProduto.setCodProduto(resultSet.getInt("CODPRODUTO"));
					iProduto.setProduto(resultSet.getString("PRODUTO"));
					ingredientes.add(iProduto);
				}
		} catch (SQLException e) {
			System.out.println("Não foi possivel Carregar os Ingredientes dos produtos: "+e.getMessage());
		}
		CloseConect();
		return ingredientes;
	}
	
	/*------------------------Seleciona Extras de um produto especifico:---------------*/
	@GET
	@Path("/SelecionaExtrasProduto/{codProduto}")
	@Produces("application/json")
	public ArrayList<ObjectExtrasProduto> SelecionaExtrasProduto(@PathParam("codProduto") String codProduto){
		ArrayList<ObjectExtrasProduto> extras = new ArrayList<ObjectExtrasProduto>();
		try {
			OpenConect();
			Select("SELECT CODPRODUTO,PRODUTO,PRECO FROM TAB_PRODUTOS "
				 + "WHERE CODPRODUTO IN "
				 	+ "(SELECT CODEXTRAS FROM TAB_PRODUTOEXTRAS WHERE CODPRODUTO="+codProduto+")");
				resultSet.beforeFirst();
				while(resultSet.next()){
					ObjectExtrasProduto eProduto = new ObjectExtrasProduto();
					eProduto.setCodProduto(resultSet.getInt("CODPRODUTO"));
					eProduto.setProduto(resultSet.getString("PRODUTO"));
					eProduto.setPreco(resultSet.getDouble("PRECO"));
					extras.add(eProduto);
				}
		} catch (SQLException e) {
			System.out.println("Não foi possivel Carregar os Ingredientes dos produtos: "+e.getMessage());
		}
		CloseConect();
		return extras;
	}	
}
